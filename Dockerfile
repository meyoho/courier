# Build the manager binary
FROM golang:1.13 as builder
# Copy in the go src and build
WORKDIR $GOPATH/src/bitbucket.org/mathildetech/courier
COPY . .
RUN CGO_ENABLED=0 go build -a -o $GOPATH/bin/manager ./cmd/manager

# Copy the controller-manager into a thin image
FROM alpine:3.11
WORKDIR /
COPY --from=builder /go/bin/manager .
# Copy the migration into courier image and install the requirements
RUN apk add --update py-pip curl
COPY --from=builder /go/src/bitbucket.org/mathildetech/courier/migration ./migration
RUN pip install --no-cache-dir -r ./migration/requirements.txt && mkdir -p /var/log/mathilde
CMD ["/manager"]
