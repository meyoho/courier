package hack

import (
	"crypto/tls"
	"encoding/base64"
	"net/http"
	"net/url"
	"time"

	"bitbucket.org/mathildetech/courier/config/envs"

	"k8s.io/client-go/rest"
)

// GetConfig generate a out cluster config for test.
func GetConfig() (*rest.Config, error) {
	// Set proxy for test
	proxyFunc := func(fixedURL *url.URL) func(*http.Request) (*url.URL, error) {
		return func(request *http.Request) (*url.URL, error) {
			auth := envs.GlobalConfig.Test.ProxyAddress
			basicAuth := "Basic " + base64.StdEncoding.EncodeToString([]byte(auth))
			request.Header.Add("Proxy-Authorization", basicAuth)
			return fixedURL, nil
		}
	}

	// Generate out cluster config for test
	cfg := &rest.Config{
		Host:        envs.GlobalConfig.Test.KubernetesEndpoint,
		BearerToken: envs.GlobalConfig.Test.KubernetesToken,
		Timeout:     time.Duration(envs.GlobalConfig.Test.KubernetesWatchTimeout) * time.Second,
	}
	if envs.GlobalConfig.Test.ProxyAddress != "null" {
		proxyUrl, err := url.Parse(envs.GlobalConfig.Test.ProxyAddress)
		if err != nil {
			return nil, err
		}
		cfg.Transport = &http.Transport{
			Proxy:           proxyFunc(proxyUrl),
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
	}
	return cfg, nil
}
