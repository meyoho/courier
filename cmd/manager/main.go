/*
Copyright 2019 Alauda AIOps Team.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"os"

	"bitbucket.org/mathildetech/courier/config/envs"
	"bitbucket.org/mathildetech/courier/hack"
	"bitbucket.org/mathildetech/courier/pkg/apis"
	"bitbucket.org/mathildetech/courier/pkg/clients/morgans"
	"bitbucket.org/mathildetech/courier/pkg/controller"
	"bitbucket.org/mathildetech/courier/pkg/cron"
	"bitbucket.org/mathildetech/courier/pkg/webhook"

	"github.com/go-logr/zapr"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/runtime/signals"
)

var (
	// LogLevel set log level for controller, flag "v", default 0
	LogLevel int
	// MetricsAddr set metrics endpoint, flag "metrics-addr", default ":8080"
	MetricsAddr string
	// ControllerNamespace is the namespace which controller deployed in, default is "alauda-system"
	ControllerNamespace string
	// WebhookEnabled enable webhook or not, flag "enable-webhook", default true
	WebhookEnabled bool
	// GCEnabled enable gc or not, flag "enable-gc", default true
	GCEnabled bool
	// LeaderElection enable leader election or not, flag "enable-leader-election", default "true"
	LeaderElection bool
	// LeaderElectionID set configmap name for election, flag "leader-election-configmap", default "courier-leader-config"
	LeaderElectionID string
	// GCInterval set gc interval, flag "gc-interval", default 12h
	GCInterval string
	// MessageTTLDays set ttl for notification message, flag "message-ttl-days", default 7
	MessageTTLDays int
	// ReportTTLDays set ttl for meter report, flag "report-ttl-days", default 7
	ReportTTLDays int
	// GCInterval set gc interval, flag "gc-interval", default 12h
	MorgansEndpoint string
	// MessageTTLDays set ttl for notification message, flag "message-ttl-days", default 7
	MorgansApiVersion string
	// ReportTTLDays set ttl for meter report, flag "report-ttl-days", default 7
	MorgansTimeout int
)

func init() {
	// Parse flags
	flag.StringVar(&MetricsAddr, "metrics-addr", ":8080", "The address the metric endpoint binds to.")
	flag.IntVar(&LogLevel, "v", 0, "Log level for courier.")
	flag.BoolVar(&GCEnabled, "enable-gc", true, "Enable gc for manager.")
	flag.BoolVar(&WebhookEnabled, "enable-webhook", true, "Enable webhooks for manager.")
	flag.BoolVar(&LeaderElection, "enable-leader-election", true, "Enable leader election for manager.")
	flag.StringVar(&LeaderElectionID, "leader-election-configmap", "courier-leader-config", "The configmap that leader election binds to.")
	flag.StringVar(&GCInterval, "gc-interval", "12h", "GC interval for manager.")
	flag.IntVar(&MessageTTLDays, "message-ttl-days", 1, "TTL days for notification message.")
	flag.IntVar(&ReportTTLDays, "report-ttl-days", 7, "TTL days for meter report.")
	flag.StringVar(&MorgansEndpoint, "morgans-endpoint", "http://morgans:8080", "The address the morgans endpoint.")
	flag.StringVar(&MorgansApiVersion, "morgans-api-version", "v1", "The api version for morgans endpoint.")
	flag.IntVar(&MorgansTimeout, "morgans-timeout", 15, "The timeout (unit:s) for morgans endpoint.")
	flag.Parse()

	// Read meta from environment variables
	ControllerNamespace = os.Getenv("NAMESPACE")
	if ControllerNamespace == "" {
		ControllerNamespace = "alauda-system"
	}

	// Initialize zap logger
	cfg := zap.NewDevelopmentConfig()
	cfg.Level = zap.NewAtomicLevelAt(zapcore.Level(-1 * LogLevel))
	zapLog, err := cfg.Build(zap.AddCallerSkip(1))
	if err != nil {
		panic("Zap build error, " + err.Error())
	}
	log.SetLogger(zapr.NewLogger(zapLog))

}

var Logger = log.Log.WithName("entrypoint")

func main() {
	// Get a config to talk to the apiserver
	Logger.Info("Setting up client for manager", "settings", envs.GlobalConfig)
	cfg, err := hack.GetConfig()
	if !envs.GlobalConfig.Test.Enabled {
		cfg, err = config.GetConfig()
	}
	if err != nil {
		Logger.Error(err, "unable to set up client config")
		os.Exit(1)
	}

	stop := signals.SetupSignalHandler()

	// Create a new Cmd to provide shared dependencies and start components
	Logger.Info("Setting up manager")
	mgr, err := manager.New(cfg, manager.Options{
		MetricsBindAddress:      MetricsAddr,
		LeaderElection:          LeaderElection,
		LeaderElectionNamespace: ControllerNamespace,
		LeaderElectionID:        LeaderElectionID,
	})
	if err != nil {
		Logger.Error(err, "unable to set up overall controller manager")
		os.Exit(1)
	}

	// Setting up clients for controller
	Logger.Info("Setting up clients")
	morgans.Init(MorgansEndpoint, MorgansApiVersion, MorgansTimeout)

	// Setup Scheme for all resources
	Logger.Info("Setting up scheme")
	if err := apis.AddToScheme(mgr.GetScheme()); err != nil {
		Logger.Error(err, "unable add APIs to scheme")
		os.Exit(1)
	}

	// Setup all Controllers
	Logger.Info("Setting up controller")
	if err := controller.AddToManager(mgr); err != nil {
		Logger.Error(err, "unable to register controllers to the manager")
		os.Exit(1)
	}

	// Setup Webhook Servers
	Logger.Info("Setting up webhooks")
	if WebhookEnabled {
		go startWebhookManager(cfg, stop)
	}

	// Start message gc
	Logger.Info("Setting up GC")
	if GCEnabled {
		if err := cron.Start(GCInterval, MessageTTLDays, ReportTTLDays); err != nil {
			Logger.Error(err, "unable to start gc")
			os.Exit(1)
		}
	}

	// Start the Cmd
	Logger.Info("Starting the Cmd")
	if err := mgr.Start(stop); err != nil {
		Logger.Error(err, "unable to register webhooks to the manager")
		os.Exit(1)
	}
}

func startWebhookManager(cfg *rest.Config, stop <-chan struct{}) {
	// Setup Webhook Servers
	Logger.Info("Setting up webhook manager")
	webhookMgr, err := manager.New(cfg, manager.Options{})
	if err != nil {
		Logger.Error(err, "unable to set up overall webhook manager")
		os.Exit(1)
	}

	if err := webhook.AddToManager(webhookMgr); err != nil {
		Logger.Error(err, "unable to register webhooks to the manager")
		os.Exit(1)
	}

	Logger.Info("Starting the webhook manager")
	if err := webhookMgr.Start(stop); err != nil {
		Logger.Error(err, "unable to run the webhook manager")
		os.Exit(1)
	}
}
