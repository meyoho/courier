#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import random
import re
import string

from infra.clients import KubernetesClient
from infra.util import get_logger, handle_exception
from infra.variables import label_type

__author__ = 'Jianyong Lian'

logger = get_logger()
version = 2


@handle_exception(logger)
def migrate():
    update_notifications()
    return True


def update_notifications():
    notifications = KubernetesClient.list_notifications()
    for item in notifications['items']:
        name = item['metadata']['name']
        namespace = item['metadata']['namespace']
        KubernetesClient.update_notification(namespace, name, item)
