## Migration 0001 / 2019-09-16
* Migrate Notification to new Notification design
* Add NotificationReceier instance to replace recipient in Notification

Old format:
```
apiVersion: aiops.alauda.io/v1beta1
kind: Notification
metadata:
  annotations:
    alauda.io/created-at: "2019-08-14T07:59:27Z"
    alauda.io/creator: admin
    alauda.io/description: jylian email
    alauda.io/display-name: jylian-tst
    alauda.io/updated-at: "2019-08-14T07:59:27Z"
  creationTimestamp: "2019-08-14T07:59:27Z"
  generation: 9
  labels:
    alauda.io/uuid: 2e34ea8d-86c3-4d08-8a95-5d6da5208a23
  name: jylian-tst
  namespace: alauda-system
  resourceVersion: "61394001"
  selfLink: /apis/aiops.alauda.io/v1beta1/namespaces/alauda-system/notifications/jylian-tst
  uid: 70119ad0-be69-11e9-8143-525400ec6688
spec:
  subscriptions:
  - method: email
    recipient: jylian@alauda.io,lianjy@alauda.io
  - method: sms
    recipient: 15051873161,15051873161
  - method: webhook
    recipient: https://cacacdc.com/router
```

New format:
```
apiVersion: aiops.alauda.io/v1beta1
kind: Notification
metadata:
  annotations:
    alauda.io/created-at: "2019-08-14T07:59:27Z"
    alauda.io/creator: admin
    alauda.io/description: jylian email
    alauda.io/display-name: jylian-tst
    alauda.io/updated-at: "2019-08-14T07:59:27Z"
  creationTimestamp: "2019-08-14T07:59:27Z"
  generation: 30
  labels:
    alauda.io/uuid: 2e34ea8d-86c3-4d08-8a95-5d6da5208a23
  name: jylian-tst
  namespace: alauda-system
  resourceVersion: "61870126"
  selfLink: /apis/aiops.alauda.io/v1beta1/namespaces/alauda-system/notifications/jylian-tst
  uid: 70119ad0-be69-11e9-8143-525400ec6688
spec:
  subscriptions:
  - method: email
    receivers:
    - name: jylian-alauda.io-2misv
      namespace: alauda-system
    - name: caadada-acadad.cn-53pfx
      namespace: alauda-system
  - method: sms
    receivers:
    - name: 1279112312312-67xb2
      namespace: alauda-system
    - name: 15051873161-2izge
      namespace: alauda-system
  - method: webhook
    receivers:
    - name: http---cdacacad.com-router-4ij1u
      namespace: alauda-system
```

## Migration 0002 / 2020-02-24
* Update notification to add template labels
