#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import re
import string

from infra.clients import KubernetesClient
from infra.util import get_logger, handle_exception
from infra.variables import label_type

__author__ = 'Jianyong Lian'

logger = get_logger()
version = 1


@handle_exception(logger)
def migrate():
    update_notifications()
    return True


def update_notifications():
    receivers = list_receivers()
    notifications = KubernetesClient.list_notifications()
    for item in notifications['items']:
        name = item['metadata']['name']
        namespace = item['metadata']['namespace']
        for subscription in item['spec']['subscriptions']:
            # replace recipient by receiver, and create receiver
            if subscription.get('recipient', ''):
                subscription['receivers'] = []
                for recipient in subscription['recipient'].split(','):
                    # if receiver already exists(same namespace/method/destination)
                    # use it instead of creating a new one
                    receiver = {'namespace': namespace,
                                'method': subscription['method'], 'destination': recipient}
                    if receiver in receivers.values():
                        for r_name, r_value in receivers.items():
                            if receiver == r_value:
                                subscription['receivers'].append(
                                    {'namespace': namespace, 'name': r_name})
                        continue
                    # if receiver does not exist, creating a new one
                    receiver_name = get_receiver_name(recipient)
                    receiver_resource = KubernetesClient.generate_receiver_resource(
                        namespace, receiver_name, recipient, subscription)
                    KubernetesClient.create_receiver(
                        namespace, receiver_name, receiver_resource)
                    subscription['receivers'].append(
                        {'namespace': namespace, 'name': receiver_name})

                    # load the new receiver to receiver dict
                    receivers[receiver_name] = receiver
                subscription.pop('recipient')
            if subscription.get('method', '') == 'email':
                subscription['template'] = 'default-email-template'
            elif subscription.get('method', '') == 'sms':
                subscription['template'] = 'default-sms-template'
            else:
                subscription['template'] = 'default-template'
        KubernetesClient.update_notification(namespace, name, item)


def get_receiver_name(name):
    name_re = re.compile('[^-a-z0-9\.]')
    prefix = name_re.sub('-', name)[:32]
    suffix = ''.join(random.sample(string.lowercase + string.digits, 5))
    return '-'.join([prefix, suffix])


def list_receivers():
    receivers_dict = {}
    receivers = KubernetesClient.list_receivers()
    for r in receivers['items']:
        r_name = r.get('metadata', {}).get('name', '')
        r_namespace = r.get('metadata', {}).get('namespace', '')
        r_method = r.get('metadata', {}).get(
            'labels', '').get(label_type, '')
        r_destination = r.get('spec', {}).get('destination', '')
        r_info = {'namespace': r_namespace,
                  'method': r_method, 'destination': r_destination}
        if r_info not in receivers_dict.values():
            receivers_dict[r_name] = {
                'namespace': r_namespace, 'method': r_method, 'destination': r_destination}

    return receivers_dict
