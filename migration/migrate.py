#!/usr/bin/env python
# -*- coding: utf-8 -*-

import click
import importlib
import os
import warnings

from infra.clients import MigrationClient
from infra.util import get_logger
from prettytable import PrettyTable

__author__ = 'Jianyong Lian'

warnings.filterwarnings("ignore")

f_logger = get_logger(to_console=False)
c_logger = get_logger(to_console=True)

namespace = os.getenv('NAMESPACE', 'alauda-system')
migration_cm = 'courier-migration-config'

versions = [
    {
        'version': 1,
        'module': 'migration_0001'
    },
    {
        'version': 2,
        'module': 'migration_0002'
    }
]


@click.command()
@click.option('--set-version', type=int, help='Set a specified version for clusters')
@click.option('--show-versions', is_flag=True, help='Show versions info for clusters')
def migrate(set_version, show_versions):
    if set_version >= 0:
        c_logger.info('Set version for migration...')
        update_version(set_version)
        c_logger.info('Current versions info ...')
        list_version()
        return

    if show_versions:
        c_logger.info('Current versions info ...')
        list_version()
        return

    c_logger.info('Before migration ...')
    list_version()
    c_logger.info('Migrate start ...')

    for v in versions:
        result = check_version(v['version'])
        if result == 1:
            continue
        if result == -1:
            break
        m = importlib.import_module(v['module'])
        if not m.migrate():
            c_logger.info('Execute migration {} failed, abort ...')
            break
        if not update_version(v['version']):
            break

    c_logger.info('After migration ...')
    list_version()
    c_logger.info('Migrate end ...')


# Check the conditions to perform the migration for a specified version
def check_version(version):
    try:
        current = MigrationClient.get_version()
        if current >= version:
            c_logger.info('current {} >= {}, skip migration {} ...'
                          .format(current, version, version))
            return 1
        if current == version - 1:
            c_logger.info('current {}, start migration to {} ...'
                          .format(current, version))
            return 0
        c_logger.info('current {} < {}, abort migration {} ...'
                      .format(current, version - 1, version))
        return -1
    except Exception as ex:
        f_logger.exception(ex)
        c_logger.info(
            'check version failed, abort migration {} ...'.format(version))
        return -1


# Update to a specified version
def update_version(version):
    try:
        c_logger.info('update version to {} ...'.format(version))
        MigrationClient.update_version(version)
        return True
    except Exception as ex:
        f_logger.exception(ex)
        c_logger.info(
            'update version failed, abort migration {} ...'.format(version))
        return False


# List versions
def list_version():
    desired = 0
    for v in versions:
        if v['version'] > desired:
            desired = v['version']
    x = PrettyTable(['current', 'desired'])
    try:
        record = {
            'current': 0,
            'desired': desired,
        }
        v = MigrationClient.get_version()
        record['current'] = v
    except Exception as ex:
        f_logger.exception(ex)
        record['current'] = '?'
    finally:
        x.add_row([record['current'], record['desired']])
    c_logger.info(x)


if __name__ == '__main__':
    migrate()
