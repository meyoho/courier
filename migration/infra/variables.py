#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

__author__ = 'Jianyong Lian'


label_type = 'type'
label_display_name = 'display-name'
label_description = 'description'


def init_variables():
    base_domain = os.getenv('LABEL_BASEDOMAIN', 'alauda.io')
    global label_type, label_display_name, label_description
    if base_domain != '':
        label_type = '/'.join([base_domain, label_type])
        label_display_name = '/'.join([base_domain,
                                       label_display_name])
        label_description = '/'.join([base_domain,
                                      label_description])
