#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Jianyong Lian'


class APIException(Exception):
    def __init__(self, response):
        self._status_code = response.status_code
        self._json = response.json()

    def __str__(self):
        return self.message

    @property
    def error_type(self):
        if self._json.get('errors', []):
            return self._json['errors'][0].get('code')
        return 'unknown_issue'

    @property
    def message(self):
        if self._json.get('errors', []):
            return self._json['errors'][0].get('message', '')
        return 'unknown message'

    @property
    def status_code(self):
        return self._status_code
