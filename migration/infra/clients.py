#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64
import json
import os
import requests

from .exceptions import APIException
from .util import get_logger
from . import variables

__author__ = 'Jianyong Lian'

logger = get_logger()

namespace = os.getenv('NAMESPACE', 'alauda-system')
pod_name = os.getenv('POD_NAME', 'courier-656448dcd4-s7459')
token_path = '/var/run/secrets/kubernetes.io/serviceaccount/token'
migration_cm = 'courier-migration-config'


class KubernetesRequest(object):
    endpoint = 'https://kubernetes.default.svc.cluster.local'
    api_v1 = '/api/v1'
    pod = api_v1 + '/namespaces/{}/pods/{}'
    secret = endpoint + api_v1 + '/namespaces/{}/secrets/{}'

    apis_clusters = '/apis/clusterregistry.k8s.io/v1alpha1'
    clusters = endpoint + apis_clusters + \
        '/namespaces/{}/clusters/'.format(namespace)

    cluster_token = ''
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'courier/v1.0'
    }

    @classmethod
    def get_cluster_token(cls):
        cluster_resources = cls._request(url=cls.clusters,
                                         headers=cls.get_headers())
        for cluster in cluster_resources['items']:
            for ep in cluster['spec'].get('kubernetesApiEndpoints', {}).get('serverEndpoints', []):
                host = ep.get('serverAddress', '')
                if host:
                    try:
                        cls._request(url=host + cls.pod.format(namespace, pod_name),
                                     headers=cls.get_headers())
                    except Exception as ex:
                        logger.exception(ex)
                        continue

                    controller = cluster['spec'].get(
                        'authInfo', {}).get('controller', {})
                    if controller:
                        kind = controller['kind']
                        if kind.lower() != 'secret':
                            raise Exception(
                                '{} not supported for cluster controller'.format(kind))

                        secret_namespace = controller['namespace']
                        secret_name = controller['name']
                        secret_resource = cls._request(url=cls.secret.format(secret_namespace, secret_name),
                                                       headers=cls.get_headers())
                        return base64.b64decode(
                            secret_resource['data']['token'])

    @classmethod
    def update_headers(cls):
        if cls.cluster_token == '':
            cls.cluster_token = cls.get_cluster_token()
        headers = {'Authorization': 'Bearer ' + cls.cluster_token}
        headers.update(cls.headers)
        return headers

    @classmethod
    def get_headers(cls):
        with open(token_path, 'r') as f:
            token = f.read()
        headers = {'Authorization': 'Bearer ' + token}
        headers.update(cls.headers)
        return headers

    @classmethod
    def _parse_response(cls, response):
        code = response.status_code
        logger.debug('Response status={}, content={}'.format(
            code, response.content))
        if 200 <= code < 300:
            return response.json() if code != 204 else None
        raise APIException(response)

    @classmethod
    def _request(cls, url, method='GET', params=None, data=None, headers=None, **kwargs):
        logger.debug(
            'Request url={}, method={}, data={}'.format(url, method, data))

        if method == 'POST':
            response = requests.post(
                url, timeout=10, params=params, json=data, headers=headers, verify=False, **kwargs)
        elif method == 'PUT':
            response = requests.put(
                url, timeout=10, params=params, json=data, headers=headers, verify=False, **kwargs)
        elif method == 'PATCH':
            response = requests.patch(
                url, timeout=10, params=params, json=data, headers=headers, verify=False, **kwargs)
        elif method == 'DELETE':
            response = requests.delete(
                url, timeout=10, params=params, headers=headers, verify=False, **kwargs)
        elif method == 'GET':
            response = requests.get(
                url, timeout=10, params=params, headers=headers, verify=False, **kwargs)
        else:
            raise Exception('method_not_implemented')
        return cls._parse_response(response)


class MigrationClient(KubernetesRequest):
    endpoint = 'https://kubernetes.default.svc.cluster.local'
    apiv1 = '/api/v1'
    configmaps = endpoint + apiv1 + \
        '/namespaces/{}/configmaps/'

    apisclusters = '/apis/clusterregistry.k8s.io/v1alpha1'
    clusters = endpoint + apisclusters + \
        '/namespaces/{}/clusters/'.format(namespace)

    apiaiops = '/apis/aiops.alauda.io/v1beta1'
    notifications = endpoint + apiaiops + '/notifications'
    notification = endpoint + apiaiops + '/namespaces/{}/notifications/{}'
    notificationreceiver = endpoint + apiaiops + \
        '/namespaces/{}/notificationreceivers/{}'

    @classmethod
    def generate_migration_resource(cls):
        resource = {'apiVersion': 'v1',
                    'kind': 'ConfigMap',
                    'metadata': {
                        'name': migration_cm,
                        'namespace': namespace
                    },
                    'data': {}
                    }

        return resource

    @classmethod
    def get_migration_resource(cls):
        return cls._request(url=cls.configmaps.format(namespace)+migration_cm,
                            headers=cls.update_headers())

    @classmethod
    def create_migration_resource(cls, data):
        return cls._request(url=cls.configmaps.format(namespace),
                            headers=cls.update_headers(),
                            method='POST',
                            data=data)

    @classmethod
    def update_migration_resource(cls, data):
        return cls._request(url=cls.configmaps.format(namespace)+migration_cm,
                            headers=cls.update_headers(),
                            method='PUT',
                            data=data)

    @classmethod
    def get_version(cls):
        try:
            resource = cls.get_migration_resource()
            return int(resource['data']['version'])
        except APIException as ex:
            if ex.status_code == 404:
                # if migration cm doesn't exist, create it with version 0
                resource = cls.generate_migration_resource()
                resource['data']['version'] = '0'
                cls.create_migration_resource(resource)
                return 0
            else:
                raise ex

    @classmethod
    def update_version(cls, version):
        version = str(version)
        try:
            resource = cls.get_migration_resource()
            resource['data']['version'] = version
            cls.update_migration_resource(resource)
        except APIException as ex:
            if ex.status_code == 404:
                # if migration cm doesn't exist, create it with version to be updated to
                resource = cls.generate_migration_resource()
                resource['data']['version'] = version
                cls.create_migration_resource(resource)
            else:
                raise ex


class KubernetesClient(KubernetesRequest):
    endpoint = 'https://kubernetes.default.svc.cluster.local'
    api_v1 = '/api/v1'
    secret = endpoint + api_v1 + '/namespaces/{}/secrets/{}'

    api_aiops = '/apis/aiops.alauda.io/v1beta1'
    notifications = endpoint + api_aiops + '/notifications'
    notification = endpoint + api_aiops + '/namespaces/{}/notifications/{}'
    notificationreceiver = endpoint + api_aiops + \
        '/namespaces/{}/notificationreceivers/'
    notificationreceivers = endpoint + api_aiops + '/notificationreceivers'

    @classmethod
    def list_notifications(cls):
        return cls._request(url=cls.notifications,
                            headers=cls.update_headers())

    @classmethod
    def update_notification(cls, namespace, name, data):
        return cls._request(url=cls.notification.format(namespace, name),
                            headers=cls.update_headers(),
                            method='PUT',
                            data=data)

    @classmethod
    def generate_receiver_resource(cls, namespace, name, receiver, data):
        destination = ''
        if not receiver.startswith('https://'):
            if data['method'] == 'dingtalk':
                destination = 'https://oapi.dingtalk.com/robot/send?access_token=' + receiver
            elif data['method'] == 'wechat':
                destination = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=' + receiver
            else:
                destination = receiver
        else:
            destination = receiver
        return {'apiVersion': 'aiops.alauda.io/v1beta1',
                'kind': 'NotificationReceiver',
                'metadata': {
                        'name': name,
                        'namespace': namespace,
                        'labels': {
                            variables.label_type: data['method']
                        },
                    'annotations': {
                            variables.label_display_name: '-'.join([data['method'], receiver]),
                            variables.label_description: ' '.join(
                                [data['method'], receiver])
                    }
                },
                'spec': {
                    'destination': destination
                }
                }

    @classmethod
    def create_receiver(cls, namespace, name, data):
        return cls._request(url=cls.notificationreceiver.format(namespace),
                            headers=cls.update_headers(),
                            method='POST',
                            data=data)

    @classmethod
    def list_receivers(cls):
        return cls._request(url=cls.notificationreceivers,
                            headers=cls.update_headers())
