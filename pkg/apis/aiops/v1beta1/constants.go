package v1beta1

import (
	"os"
	"strings"

	"bitbucket.org/mathildetech/courier/config/envs"
)

var ControllerNamespace string

const (
	// Available notification methods
	MethodWebhook  = "webhook"
	MethodEmail    = "email"
	MethodSMS      = "sms"
	MethodDingtalk = "dingtalk"
	MethodWechat   = "wechat"

	// Type for notification sender secret
	SecretTypeNotificationSender = "NotificationSender"

	// Default notification settings
	DefaultEmailSender   = "default-email-sender"
	DefaultSMSSender     = "default-sms-sender"
	DefaultTemplate      = "default-template"
	DefaultSMSTemplateID = "462079"
)

var (
	// Common annotations keys
	AnnotationKeyDisplayName           = "display-name"
	AnnotationKeySynchronizationResult = "synchronization-result"

	// Common label keys
	LabelKeyOwner             = "owner"
	LabelKeyServer            = "server"
	LabelKeyType              = "type"
	LabelKeyVersion           = "version"
	LabelKeyUniqueName        = "unique-name"
	LabelKeyDestinationMD5    = "destination-md5"
	LabelKeyDisplayNameMD5    = "display-name-md5"
	LabelNotificationTemplate = "notificationtemplate/%s"
)

func init() {
	if envs.GlobalConfig.Label.BaseDomain != "" {
		AnnotationKeyDisplayName = strings.Join([]string{envs.GlobalConfig.Label.BaseDomain, AnnotationKeyDisplayName}, "/")
		AnnotationKeySynchronizationResult = strings.Join([]string{envs.GlobalConfig.Label.BaseDomain, AnnotationKeySynchronizationResult}, "/")
		LabelKeyOwner = strings.Join([]string{envs.GlobalConfig.Label.BaseDomain, LabelKeyOwner}, "/")
		LabelKeyServer = strings.Join([]string{envs.GlobalConfig.Label.BaseDomain, LabelKeyServer}, "/")
		LabelKeyType = strings.Join([]string{envs.GlobalConfig.Label.BaseDomain, LabelKeyType}, "/")
		LabelKeyVersion = strings.Join([]string{envs.GlobalConfig.Label.BaseDomain, LabelKeyVersion}, "/")
		LabelKeyUniqueName = strings.Join([]string{envs.GlobalConfig.Label.BaseDomain, LabelKeyUniqueName}, "/")
		LabelKeyDestinationMD5 = strings.Join([]string{envs.GlobalConfig.Label.BaseDomain, LabelKeyDestinationMD5}, "/")
		LabelKeyDisplayNameMD5 = strings.Join([]string{envs.GlobalConfig.Label.BaseDomain, LabelKeyDisplayNameMD5}, "/")
		LabelNotificationTemplate = strings.Replace(LabelNotificationTemplate, "/", "."+envs.GlobalConfig.Label.BaseDomain+"/", -1)
	}

	ControllerNamespace = os.Getenv("NAMESPACE")
	if ControllerNamespace == "" {
		ControllerNamespace = "alauda-system"
	}
}
