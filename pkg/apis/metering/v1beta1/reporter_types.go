/*
Copyright 2019 Alauda AIOps Team.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ConditionStatus string

type ConditionType string

// These are valid conditions of meter report. Currently, ready is the only condition type
// for meter report, it means that whether this report has been generated successfully.
const (
	// ReporterReady this report has been generated successfully
	ReporterReady ConditionType = "Ready"
	// Report condition status
	ConditionTrue    ConditionStatus = "True"
	ConditionFalse   ConditionStatus = "False"
	ConditionUnknown ConditionStatus = "Unknown"
	// Report status phase
	PhasePending    string = "Pending"
	PhaseProcessing string = "Processing"
	PhaseCompleted  string = "Completed"
	PhaseFailed     string = "Failed"
)

// ReportSpec defines the desired state of Report
type ReportSpec struct {
	// Kind for this report, one of summary, detail.
	Kind string `json:"kind"`
	// Type for meter, one of podUsage, podRequests, namespaceQuota, projectQuota.
	Type string `json:"type"`
	// GroupBy for report, one of namespace, project.
	GroupBy string `json:"groupBy"`
	// StartTime for this report, defines the scope for meter.
	StartTime metav1.Time `json:"startTime"`
	// EndTime for this report, defines the scope for meter.
	EndTime metav1.Time `json:"endTime"`
	// Projects defines filters for project, empty means all.
	// +optional
	Projects []string `json:"projects,omitempty"`
	// Clusters defines filters for cluster, empty means all.
	// +optional
	Clusters []string `json:"clusters,omitempty"`
	// Namespaces defines filters for namespace, empty means all.
	// +optional
	Namespaces []string `json:"namespaces,omitempty"`
	// OrderBy defines field for for sort, default by name.
	// +optional
	OrderBy string `json:"orderBy,omitempty"`
}

// ReportStatus defines the observed state of Report
type ReportStatus struct {
	Conditions   []ReportCondition `json:"conditions,omitempty"`
	Phase        string            `json:"phase,omitempty"`
	DownloadLink string            `json:"downloadLink,omitempty"`
}

// ReportCondition contains condition information for a meter report.
type ReportCondition struct {
	// Type of meter report condition.
	Type ConditionType `json:"type"`
	// Status of the condition, one of True, False, Unknown.
	Status ConditionStatus `json:"status"`
	// Retry times for this notification message.
	// +optional
	RetryTimes int `json:"retryTimes,omitempty"`
	// Last time the condition probe from controller.
	// +optional
	LastProbeTime metav1.Time `json:"lastProbeTime,omitempty"`
	// Last time the condition transit from one status to another.
	// +optional
	LastTransitionTime metav1.Time `json:"lastTransitionTime,omitempty"`
	// Brief reason for the condition's last transition.
	// +optional
	Reason string `json:"reason,omitempty"`
	// Human readable message indicating details about last transition.
	// +optional
	Message string `json:"message,omitempty"`
}

// Report is the Schema for the templates API
// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +k8s:openapi-gen=true
// +genclient:nonNamespaced
// +kubebuilder:subresource:status
// +kubebuilder:resource:shortName=mr
type Report struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              ReportSpec   `json:"spec"`
	Status            ReportStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ReportList contains a list of Report
type ReportList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Report `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Report{}, &ReportList{})
}
