package cron

import (
	"github.com/robfig/cron/v3"
	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
)

var (
	Logger = log.Log.WithName("gc")
	// GCInterval set gc interval
	GCInterval string
	// MessageTTLDays set ttl for notification message
	MessageTTLDays int
	// ReportTTLDays set ttl for meter report
	ReportTTLDays int
)

func Start(gcInterval string, messageTTLDays int, reportTTLDays int) error {
	GCInterval = gcInterval
	MessageTTLDays = messageTTLDays
	ReportTTLDays = reportTTLDays
	if err := startMessageGC(); err != nil {
		return err
	}
	if err := startReportGC(); err != nil {
		return err
	}
	return nil
}

func startMessageGC() error {
	c := cron.New()
	mgc, err := NewMessageGC(MessageTTLDays)
	if err != nil {
		return err
	}
	_, err = c.AddJob("@every "+GCInterval, mgc)
	if err != nil {
		return err
	}
	go c.Start()
	return nil
}

func startReportGC() error {
	c := cron.New()
	mgc, err := NewReportGC(ReportTTLDays)
	if err != nil {
		return err
	}
	_, err = c.AddJob("@every "+GCInterval, mgc)
	if err != nil {
		return err
	}
	go c.Start()
	return nil
}
