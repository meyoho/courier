package cron

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"bitbucket.org/mathildetech/courier/config/envs"
	"bitbucket.org/mathildetech/courier/hack"
	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"

	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
)

type messageGC struct {
	ttl    int
	client client.Client
}

func NewMessageGC(ttl int) (*messageGC, error) {
	cfg, err := hack.GetConfig()
	if !envs.GlobalConfig.Test.Enabled {
		cfg, err = config.GetConfig()
	}
	if err != nil {
		return nil, err
	}
	clt, err := client.New(cfg, client.Options{})
	if err != nil {
		Logger.Error(err, "unable to set up client config")
		return nil, err
	}
	return &messageGC{
		ttl:    ttl,
		client: clt,
	}, nil
}

func (g *messageGC) Run() {
	// Sleep random minutes to avoid multiple instance conflict
	rand.Seed(time.Now().Unix())
	minutes := rand.Intn(30)
	Logger.Info(fmt.Sprintf("Step in notification message gc, sleep %d minutes to avoid conflict", minutes))
	time.Sleep(time.Minute * time.Duration(minutes))
	// Try to delete unused notification messages
	deleted := int64(0)
	instances := &aiopsv1beta1.NotificationMessageList{}
	for {
		options := &client.ListOptions{
			Raw: &v1.ListOptions{
				Limit:    500,
				Continue: instances.Continue,
			},
		}
		err := g.client.List(context.Background(), options, instances)
		if err != nil {
			Logger.Error(err, "List message error, abort")
			return
		}
		t := v1.NewTime(time.Now().Add(-time.Hour * 24 * time.Duration(g.ttl)))
		for _, i := range instances.Items {
			if i.CreationTimestamp.Before(&t) {
				deleted++
				Logger.Info("Try to delete message", "message", i.Namespace+"/"+i.Name)
				if err = g.client.Delete(context.Background(), &i, client.GracePeriodSeconds(30)); err != nil {
					Logger.Error(err, "Delete message error, skip", "message", i.Namespace+"/"+i.Name)
				}
			}
		}
		if instances.Continue == "" {
			break
		}
	}
	Logger.Info(fmt.Sprintf("Step out notification message gc, deleted %d notification messages", deleted))
}
