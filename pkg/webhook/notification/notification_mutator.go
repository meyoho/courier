package notification

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"
	"bitbucket.org/mathildetech/courier/pkg/webhook/tools"

	mapset "github.com/deckarep/golang-set"
	admissionregistrationv1beta1 "k8s.io/api/admissionregistration/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/runtime/inject"
	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/builder"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/types"
)

var Logger = log.Log.WithName("notification-webhook")

type notificationMutator struct {
	client  client.Client
	decoder types.Decoder
}

func AddMutator(mgr manager.Manager) (webhook.Webhook, error) {
	return newMutator(mgr)
}

// newMutator returns a new mutator for notification
func newMutator(mgr manager.Manager) (webhook.Webhook, error) {
	return builder.NewWebhookBuilder().
		Name("aiops.notification.mutating.alauda.io").
		Mutating().
		Operations(admissionregistrationv1beta1.Create, admissionregistrationv1beta1.Update).
		WithManager(mgr).
		ForType(&aiopsv1beta1.Notification{}).
		Handlers(&notificationMutator{}).
		Path("/mutate-notifications").
		FailurePolicy(admissionregistrationv1beta1.Fail).
		Build()
}

var _ admission.Handler = &notificationMutator{}

func (w *notificationMutator) Handle(ctx context.Context, req types.Request) types.Response {
	instance := &aiopsv1beta1.Notification{}
	if err := w.decoder.Decode(req, instance); err != nil {
		return tools.ValidationResponse(false, http.StatusBadRequest, metav1.StatusReasonBadRequest, err.Error())
	}
	Logger.Info("Step into aiops mutating webhook handle", "notification", instance.Namespace+"/"+instance.Name)
	// Add default settings for notification and version label
	mirror := instance.DeepCopy()
	if mirror.Labels == nil {
		mirror.Labels = map[string]string{}
	}
	for i, s := range mirror.Spec.Subscriptions {
		if s.Template == "" {
			mirror.Spec.Subscriptions[i].Template = aiopsv1beta1.DefaultTemplate
		}
		if s.Sender == "" {
			if s.Method == aiopsv1beta1.MethodEmail {
				mirror.Spec.Subscriptions[i].Sender = aiopsv1beta1.DefaultEmailSender
			}
			if s.Method == aiopsv1beta1.MethodSMS {
				mirror.Spec.Subscriptions[i].Sender = aiopsv1beta1.DefaultSMSSender
			}
		}
		if s.Recipient != "" {
			mirror.Labels[aiopsv1beta1.LabelKeyVersion] = "1.0"
		} else {
			delete(mirror.Labels, aiopsv1beta1.LabelKeyVersion)
		}
	}

	// Update template label for notification
	mirror, err := w.UpdateTemplateLabels(mirror, req)
	if err != nil {
		return tools.ValidationResponse(false, http.StatusInternalServerError, metav1.StatusReasonInternalError, err.Error())
	}
	Logger.Info("Step out aiops mutating webhook handle", "notification", instance.Namespace+"/"+instance.Name)
	return admission.PatchResponse(instance, mirror)
}

func (w *notificationMutator) UpdateTemplateLabels(mirror *aiopsv1beta1.Notification, req types.Request) (*aiopsv1beta1.Notification, error) {
	templateList := mapset.NewSet()
	for i := range mirror.Spec.Subscriptions {
		templateList.Add(mirror.Spec.Subscriptions[i].Template)
	}
	Logger.V(2).Info("Update template label for notification", "notification", mirror.Namespace+"/"+mirror.Name, "new templates", fmt.Sprint(templateList.ToSlice()))

	oldTemplateList := mapset.NewSet()
	if req.AdmissionRequest.OldObject.Size() != 0 {
		oldNotification := &aiopsv1beta1.Notification{}
		if err := json.Unmarshal(req.AdmissionRequest.OldObject.Raw, oldNotification); err != nil {
			Logger.Error(err, "Unmarshal old notification error", "notification", mirror.Namespace+"/"+mirror.Name)
			return nil, err
		}
		for i := range oldNotification.Spec.Subscriptions {
			oldTemplateList.Add(oldNotification.Spec.Subscriptions[i].Template)
		}
	}
	Logger.V(2).Info("Update template labels for notification", "notification", mirror.Namespace+"/"+mirror.Name, "old templates", fmt.Sprint(oldTemplateList.ToSlice()))

	toAdd := templateList.Difference(oldTemplateList).ToSlice()
	toUpdate := templateList.Intersect(oldTemplateList).ToSlice()
	toRemove := oldTemplateList.Difference(templateList).ToSlice()
	Logger.V(2).Info("Update template labels for notification", "notification", mirror.Namespace+"/"+mirror.Name, "add label", fmt.Sprint(toAdd))
	Logger.V(2).Info("Update template labels for notification", "notification", mirror.Namespace+"/"+mirror.Name, "update label", fmt.Sprint(toUpdate))
	Logger.V(2).Info("Update template labels for notification", "notification", mirror.Namespace+"/"+mirror.Name, "remove label", fmt.Sprint(toRemove))
	for _, t := range append(toAdd, toUpdate...) {
		mirror.Labels[fmt.Sprintf(aiopsv1beta1.LabelNotificationTemplate, t)] = "true"
	}
	for _, t := range toRemove {
		delete(mirror.Labels, fmt.Sprintf(aiopsv1beta1.LabelNotificationTemplate, t))
	}
	return mirror, nil
}

var _ inject.Client = &notificationMutator{}

// InjectClient injects the client into the podAnnotator
func (w *notificationMutator) InjectClient(c client.Client) error {
	w.client = c
	return nil
}

var _ inject.Decoder = &notificationMutator{}

// InjectDecoder injects the decoder into the podAnnotator
func (w *notificationMutator) InjectDecoder(d types.Decoder) error {
	w.decoder = d
	return nil
}
