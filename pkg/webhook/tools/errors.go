package tools

import (
	admissionv1beta1 "k8s.io/api/admission/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/types"
)

// ValidationResponse returns a response for admitting a request.
func ValidationResponse(allowed bool, code int32, reason metav1.StatusReason, message string) types.Response {
	resp := types.Response{
		Response: &admissionv1beta1.AdmissionResponse{
			Allowed: allowed,
		},
	}
	if len(reason) > 0 {
		resp.Response.Result = &metav1.Status{
			Code:    code,
			Reason:  reason,
			Message: message,
		}
	}
	return resp
}
