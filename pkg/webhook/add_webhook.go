package webhook

import (
	"bitbucket.org/mathildetech/courier/pkg/webhook/notification"
	"bitbucket.org/mathildetech/courier/pkg/webhook/receiver"
	"bitbucket.org/mathildetech/courier/pkg/webhook/sender"
)

func init() {
	// AddToManagerFuncs is a list of functions to create webhooks and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, notification.AddMutator, receiver.AddMutator, sender.AddValidator, sender.AddMutator)
}
