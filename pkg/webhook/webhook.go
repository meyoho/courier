/*
Copyright 2019 Alauda AIOps Team.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package webhook

import (
	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"

	apitypes "k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

// AddToManagerFuncs is a list of functions to add all Controllers to the Manager
var AddToManagerFuncs []func(manager manager.Manager) (webhook.Webhook, error)

// AddToManager adds all Controllers to the Manager
// +kubebuilder:rbac:groups=admissionregistration.k8s.io,resources=mutatingwebhookconfigurations;validatingwebhookconfigurations,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=services,verbs=get;list;watch;create;update;patch;delete
func AddToManager(m manager.Manager) error {
	svr, err := webhook.NewServer("aiops-courier-admission-server", m, webhook.ServerOptions{
		Port:    int32(443),
		CertDir: "/cert",
		BootstrapOptions: &webhook.BootstrapOptions{
			Secret: &apitypes.NamespacedName{
				Namespace: aiopsv1beta1.ControllerNamespace,
				Name:      "aiops-courier-admission-server-secret",
			},
			Service: &webhook.Service{
				Namespace: aiopsv1beta1.ControllerNamespace,
				Name:      "courier",
				// Selectors should select the pods that runs this webhook server.
				Selectors: map[string]string{
					"service_name": "courier",
				},
			},
			MutatingWebhookConfigName:   "courier",
			ValidatingWebhookConfigName: "courier",
		},
	})
	if err != nil {
		return err
	}

	// Add webhooks
	var webhooks []webhook.Webhook
	for _, f := range AddToManagerFuncs {
		w, err := f(m)
		if err != nil {
			return err
		}
		webhooks = append(webhooks, w)
	}
	if err = svr.Register(webhooks...); err != nil {
		return err
	}
	return nil
}
