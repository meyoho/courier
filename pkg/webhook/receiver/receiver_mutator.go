package receiver

import (
	"context"
	"crypto/md5"
	"fmt"
	"net/http"

	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"
	"bitbucket.org/mathildetech/courier/pkg/webhook/tools"

	admissionregistrationv1beta1 "k8s.io/api/admissionregistration/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/runtime/inject"
	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/builder"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/types"
)

var Logger = log.Log.WithName("receiver-webhook")

type receiverMutator struct {
	client  client.Client
	decoder types.Decoder
}

func AddMutator(mgr manager.Manager) (webhook.Webhook, error) {
	return newMutator(mgr)
}

// newValidator returns a new validator for notification receiver
func newMutator(mgr manager.Manager) (webhook.Webhook, error) {
	mutatingWebhook, err := builder.NewWebhookBuilder().
		Name("aiops.receiver.mutating.alauda.io").
		Mutating().
		Operations(admissionregistrationv1beta1.Create, admissionregistrationv1beta1.Update).
		WithManager(mgr).
		ForType(&aiopsv1beta1.NotificationReceiver{}).
		Handlers(&receiverMutator{}).
		Path("/mutate-receivers").
		FailurePolicy(admissionregistrationv1beta1.Ignore).
		Build()
	return mutatingWebhook, err
}

var _ admission.Handler = &receiverMutator{}

func (w *receiverMutator) Handle(ctx context.Context, req types.Request) types.Response {
	instance := &aiopsv1beta1.NotificationReceiver{}
	err := w.decoder.Decode(req, instance)
	if err != nil {
		return tools.ValidationResponse(false, http.StatusBadRequest, metav1.StatusReasonBadRequest, err.Error())
	}
	Logger.Info("Step into aiops mutating webhook handle", "receiver", instance.Namespace+"/"+instance.Name)
	mirror := instance.DeepCopy()
	if mirror.Annotations == nil {
		mirror.Annotations = map[string]string{}
	}
	if mirror.Labels == nil {
		mirror.Labels = map[string]string{}
	}

	displayName := mirror.Annotations[aiopsv1beta1.AnnotationKeyDisplayName]
	mirror.Labels[aiopsv1beta1.LabelKeyDisplayNameMD5] = fmt.Sprintf("%x", md5.Sum([]byte(displayName)))
	destination := mirror.Spec.Destination
	mirror.Labels[aiopsv1beta1.LabelKeyDestinationMD5] = fmt.Sprintf("%x", md5.Sum([]byte(destination)))
	Logger.Info("Step out aiops mutating webhook handle", "receiver", instance.Namespace+"/"+instance.Name)
	return admission.PatchResponse(instance, mirror)
}

var _ inject.Client = &receiverMutator{}

// InjectClient injects the client into the podAnnotator
func (w *receiverMutator) InjectClient(c client.Client) error {
	w.client = c
	return nil
}

var _ inject.Decoder = &receiverMutator{}

// InjectDecoder injects the decoder into the podAnnotator
func (w *receiverMutator) InjectDecoder(d types.Decoder) error {
	w.decoder = d
	return nil
}
