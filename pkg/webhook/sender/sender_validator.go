package sender

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"

	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"
	"bitbucket.org/mathildetech/courier/pkg/webhook/tools"

	"gopkg.in/gomail.v2"
	admissionregistrationv1beta1 "k8s.io/api/admissionregistration/v1beta1"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	apitypes "k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/runtime/inject"
	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/builder"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/types"
)

var Logger = log.Log.WithName("sender-webhook")

type senderValidator struct {
	client  client.Client
	decoder types.Decoder
}

func AddValidator(mgr manager.Manager) (webhook.Webhook, error) {
	return newValidator(mgr)
}

// newValidator returns a new validator for notification sender
func newValidator(mgr manager.Manager) (webhook.Webhook, error) {
	return builder.NewWebhookBuilder().
		Name("aiops.sender.validating.alauda.io").
		Validating().
		Operations(admissionregistrationv1beta1.Create, admissionregistrationv1beta1.Update).
		WithManager(mgr).
		ForType(&v1.Secret{}).
		Handlers(&senderValidator{}).
		Path("/validate-senders").
		FailurePolicy(admissionregistrationv1beta1.Ignore).
		Build()
}

var _ admission.Handler = &senderValidator{}

func (w *senderValidator) Handle(ctx context.Context, req types.Request) types.Response {
	secret := &v1.Secret{}
	if err := w.decoder.Decode(req, secret); err != nil {
		return tools.ValidationResponse(false, http.StatusBadRequest, metav1.StatusReasonBadRequest, err.Error())
	}
	Logger.Info("Step into aiops validating webhook handle", "secret", secret.Namespace+"/"+secret.Name)
	if secret.Type != aiopsv1beta1.SecretTypeNotificationSender {
		return admission.ValidationResponse(true, "")
	}
	// Validate type and associated server in secret labels
	method := secret.Labels[aiopsv1beta1.LabelKeyType]
	if method != aiopsv1beta1.MethodWebhook && method != aiopsv1beta1.MethodEmail && method != aiopsv1beta1.MethodSMS {
		Logger.Info(fmt.Sprintf("secret label value for %s is invalid", aiopsv1beta1.LabelKeyType), "type", method, "secret", secret.Namespace+"/"+secret.Name)
		return tools.ValidationResponse(false, http.StatusBadRequest, metav1.StatusReasonInvalid, fmt.Sprintf("value for label %s is invalid", aiopsv1beta1.LabelKeyType))
	}
	server := secret.Labels[aiopsv1beta1.LabelKeyServer]
	if server == "" && method != aiopsv1beta1.MethodWebhook {
		Logger.Info(fmt.Sprintf("secret label value for %s is invalid", aiopsv1beta1.LabelKeyServer), "server", server, "secret", secret.Namespace+"/"+secret.Name)
		return tools.ValidationResponse(false, http.StatusBadRequest, metav1.StatusReasonInvalid, fmt.Sprintf("%s label is required", aiopsv1beta1.LabelKeyServer))
	}
	// Validate unique name and availability
	if err := w.ValidateUnique(secret); err != nil {
		Logger.Error(err, "unique validation failed", "secret", secret.Namespace+"/"+secret.Name)
		return tools.ValidationResponse(false, http.StatusBadRequest, metav1.StatusReasonInvalid, fmt.Sprintf("notifiaction sender unique validation error: "+err.Error()))
	}
	if err := w.ValidateAvailability(secret); err != nil {
		Logger.Error(err, "availability validation failed", "secret", secret.Namespace+"/"+secret.Name)
		return tools.ValidationResponse(false, http.StatusBadRequest, metav1.StatusReasonInvalid, fmt.Sprintf("notification sender availability validation error: "+err.Error()))
	}
	Logger.Info("Step out aiops validating webhook handle", "secret", secret.Namespace+"/"+secret.Name)
	return admission.ValidationResponse(true, "")
}

func (w *senderValidator) ValidateUnique(secret *v1.Secret) error {
	secrets := &v1.SecretList{}
	uniqueName := secret.Labels[aiopsv1beta1.LabelKeyUniqueName]
	requirement, _ := labels.NewRequirement(aiopsv1beta1.LabelKeyUniqueName, "==", []string{uniqueName})
	options := &client.ListOptions{
		Namespace:     aiopsv1beta1.ControllerNamespace,
		LabelSelector: labels.NewSelector().Add(*requirement),
	}
	if err := w.client.List(context.Background(), options, secrets); err != nil {
		return fmt.Errorf("list secret error with unique name selector error, %s", err.Error())
	}
	if len(secrets.Items) > 0 && secrets.Items[0].Name != secret.Name {
		return fmt.Errorf("a duplicated secret %s/%s has already exists", secrets.Items[0].Namespace, secrets.Items[0].Name)
	}
	return nil
}

func (w *senderValidator) ValidateAvailability(secret *v1.Secret) error {
	if secret.Labels[aiopsv1beta1.LabelKeyType] != aiopsv1beta1.MethodEmail {
		return nil
	}
	serverName := secret.Labels[aiopsv1beta1.LabelKeyServer]
	server := &aiopsv1beta1.NotificationServer{}
	if err := w.client.Get(context.Background(), apitypes.NamespacedName{Name: serverName}, server); err != nil {
		return fmt.Errorf("get notification server error, " + err.Error())
	}
	d := &gomail.Dialer{
		Host:     server.Spec.Email.Host,
		Port:     int(server.Spec.Email.Port),
		Username: string(secret.Data["username"]),
		Password: string(secret.Data["password"]),
		SSL:      server.Spec.Email.SslEnabled,
	}
	if server.Spec.Email.InsecureSkipVerify {
		d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	}
	closer, err := d.Dial()
	if err == nil {
		if err := closer.Close(); err != nil {
			Logger.Error(err, "dial connection close error", "secret", secret.Namespace+"/"+secret.Name)
		}
	}
	return err
}

var _ inject.Client = &senderValidator{}

// InjectClient injects the client into the podAnnotator
func (w *senderValidator) InjectClient(c client.Client) error {
	w.client = c
	return nil
}

var _ inject.Decoder = &senderValidator{}

// InjectDecoder injects the decoder into the podAnnotator
func (w *senderValidator) InjectDecoder(d types.Decoder) error {
	w.decoder = d
	return nil
}
