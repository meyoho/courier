package sender

import (
	"bytes"
	"context"
	"crypto/md5"
	"fmt"
	"net/http"

	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"
	"bitbucket.org/mathildetech/courier/pkg/webhook/tools"

	admissionregistrationv1beta1 "k8s.io/api/admissionregistration/v1beta1"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/runtime/inject"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/builder"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission/types"
)

type senderMutator struct {
	client  client.Client
	decoder types.Decoder
}

func AddMutator(mgr manager.Manager) (webhook.Webhook, error) {
	return newMutator(mgr)
}

// newMutator returns a new mutator for notification sender
func newMutator(mgr manager.Manager) (webhook.Webhook, error) {
	return builder.NewWebhookBuilder().
		Name("aiops.sender.mutating.alauda.io").
		Mutating().
		Operations(admissionregistrationv1beta1.Create, admissionregistrationv1beta1.Update).
		WithManager(mgr).
		ForType(&v1.Secret{}).
		Handlers(&senderMutator{}).
		Path("/mutate-senders").
		FailurePolicy(admissionregistrationv1beta1.Ignore).
		Build()
}

var _ admission.Handler = &senderMutator{}

func (w *senderMutator) Handle(ctx context.Context, req types.Request) types.Response {
	secret := &v1.Secret{}
	if err := w.decoder.Decode(req, secret); err != nil {
		return tools.ValidationResponse(false, http.StatusBadRequest, metav1.StatusReasonBadRequest, err.Error())
	}
	Logger.Info("Step into aiops mutating webhook handle", "secret", secret.Namespace+"/"+secret.Name)
	if secret.Type != aiopsv1beta1.SecretTypeNotificationSender {
		return admission.ValidationResponse(true, "")
	}
	// Generate unique name for notification sender
	uniqueName := w.GetUniqueName(secret)
	mirror := secret.DeepCopy()
	if mirror.Labels == nil {
		mirror.Labels = map[string]string{}
	}
	mirror.Labels[aiopsv1beta1.LabelKeyUniqueName] = uniqueName
	Logger.Info("Step out aiops mutating webhook handle", "secret", secret.Namespace+"/"+secret.Name)
	return admission.PatchResponse(secret, mirror)
}

func (w *senderMutator) GetUniqueName(secret *v1.Secret) string {
	bytesArray := make([][]byte, 0)
	for _, v := range secret.Data {
		bytesArray = append(bytesArray, v)
	}
	bytesUnique := bytes.Join(bytesArray, []byte("__"))
	uniqueName := fmt.Sprintf("%x", md5.Sum(bytesUnique))
	if secret.Labels[aiopsv1beta1.LabelKeyType] == aiopsv1beta1.MethodEmail {
		uniqueName = fmt.Sprintf("%x", md5.Sum(secret.Data["username"]))
	}
	return uniqueName
}

var _ inject.Client = &senderMutator{}

// InjectClient injects the client into the podAnnotator
func (w *senderMutator) InjectClient(c client.Client) error {
	w.client = c
	return nil
}

var _ inject.Decoder = &senderMutator{}

// InjectDecoder injects the decoder into the podAnnotator
func (w *senderMutator) InjectDecoder(d types.Decoder) error {
	w.decoder = d
	return nil
}
