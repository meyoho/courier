package operators

import (
	"bytes"
	"encoding/json"
	"strings"
	"text/template"

	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"
	"bitbucket.org/mathildetech/courier/pkg/operators/tools/adapter"
	"bitbucket.org/mathildetech/courier/pkg/operators/tools/utils"

	"k8s.io/api/core/v1"
)

type Content struct {
	Content string `json:"content"`
}

type Body struct {
	MsgType string  `json:"msgtype"`
	Text    Content `json:"text"`
}

type NotificationTask struct {
	Notification *aiopsv1beta1.Notification
	Message      *aiopsv1beta1.NotificationMessage
	Reference    *NotificationReference
}

type NotificationReference struct {
	Templates []*aiopsv1beta1.NotificationTemplate
	Servers   []*aiopsv1beta1.NotificationServer
	Receivers []*aiopsv1beta1.NotificationReceiver
	Secrets   []*v1.Secret
}

type NotificationSubTask struct {
	Method       string
	Notification *aiopsv1beta1.Notification
	Message      *aiopsv1beta1.NotificationMessage
	Template     *aiopsv1beta1.NotificationTemplate
	Sender       *v1.Secret
	Server       *aiopsv1beta1.NotificationServer
	Receivers    []*aiopsv1beta1.NotificationReceiver
	SenderResult []*adapter.SenderResult
	HasDone      bool
}

func (t *NotificationTask) GetReceivers(s aiopsv1beta1.NotificationSubscription) []*aiopsv1beta1.NotificationReceiver {
	var receivers []*aiopsv1beta1.NotificationReceiver
	for _, d := range s.Receivers {
		for _, r := range t.Reference.Receivers {
			if r.Namespace == d.Namespace && r.Name == d.Name {
				receivers = append(receivers, r)
				break
			}
		}
	}
	if len(s.Recipient) == 0 {
		return receivers
	}
	// Try to transition recipient to notification receivers
	if s.Method != aiopsv1beta1.MethodEmail {
		receiver := &aiopsv1beta1.NotificationReceiver{Spec: aiopsv1beta1.NotificationReceiverSpec{Destination: s.Recipient}}
		receivers = append(receivers, receiver)
		return receivers
	}
	for _, e := range strings.Split(s.Recipient, ",") {
		receiver := &aiopsv1beta1.NotificationReceiver{Spec: aiopsv1beta1.NotificationReceiverSpec{Destination: e}}
		receivers = append(receivers, receiver)
	}
	return receivers
}

func (t *NotificationTask) GetSender(s aiopsv1beta1.NotificationSubscription) *v1.Secret {
	if s.Sender == "" {
		return nil
	}
	for _, secret := range t.Reference.Secrets {
		if secret.Name == s.Sender && secret.Namespace == aiopsv1beta1.ControllerNamespace {
			return secret
		}
	}
	return nil
}

func (t *NotificationTask) GetServer(s aiopsv1beta1.NotificationSubscription) *aiopsv1beta1.NotificationServer {
	sender := t.GetSender(s)
	if sender == nil {
		return nil
	}
	server := sender.Labels[aiopsv1beta1.LabelKeyServer]
	if server == "" {
		return nil
	}
	for _, i := range t.Reference.Servers {
		if i.Name == server {
			return i
		}
	}
	return nil
}

func (t *NotificationTask) GetTemplate(s aiopsv1beta1.NotificationSubscription) *aiopsv1beta1.NotificationTemplate {
	if s.Template == "" {
		return nil
	}
	for _, t := range t.Reference.Templates {
		if t.Name == s.Template {
			return t
		}
	}
	return nil
}

func (st *NotificationSubTask) GetSubject() (string, error) {
	t, err := template.New("").Funcs(template.FuncMap(utils.CustomFuncs)).Parse(st.Template.Spec.Subject)
	if err != nil {
		return "", err
	}

	var body interface{}
	if err = json.Unmarshal(st.Message.Spec.Body.Raw, &body); err != nil {
		return "", err
	}

	var buf bytes.Buffer
	err = t.Execute(&buf, body)
	return buf.String(), err
}

func (st *NotificationSubTask) GetContent() (string, error) {
	t, err := template.New("").Funcs(template.FuncMap(utils.CustomFuncs)).Parse(st.Template.Spec.Content)
	if err != nil {
		return "", err
	}

	var body interface{}
	if err = json.Unmarshal(st.Message.Spec.Body.Raw, &body); err != nil {
		return "", err
	}

	var buf bytes.Buffer
	err = t.Execute(&buf, body)
	return buf.String(), err
}
