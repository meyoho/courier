package operators

import (
	"encoding/json"

	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"

	"bitbucket.org/mathildetech/courier/pkg/operators/tools/adapter"
	"bitbucket.org/mathildetech/courier/pkg/operators/tools/email"
	"bitbucket.org/mathildetech/courier/pkg/operators/tools/sms"
	"bitbucket.org/mathildetech/courier/pkg/operators/tools/webhook"
)

type Adapter interface {
	Send() ([]*adapter.SenderResult, error)
}

func (st *NotificationSubTask) NewEmailAdapter() (Adapter, error) {
	sender := &email.Sender{
		Username:           string(st.Sender.Data["username"]),
		Password:           string(st.Sender.Data["password"]),
		Host:               st.Server.Spec.Email.Host,
		Port:               int(st.Server.Spec.Email.Port),
		SSL:                st.Server.Spec.Email.SslEnabled,
		InsecureSkipVerify: st.Server.Spec.Email.InsecureSkipVerify,
	}

	body, err := st.GetContent()
	if err != nil {
		Logger.Error(err, "Failed to get content for email")
		return nil, err
	}
	subject, err := st.GetSubject()
	if err != nil {
		Logger.Error(err, "Failed to get subject for email")
		return nil, err
	}
	content := &email.Content{
		Body: body,
		Type: email.TextContentType,
	}
	return &adapter.EmailAdapter{
		Email:     email.NewEmail(sender, subject, content, nil),
		Receivers: st.Receivers,
	}, nil
}

func (st *NotificationSubTask) NewSMSAdapter() (Adapter, error) {
	sender := &sms.Sender{
		Host:         st.Server.Spec.Sms.Host,
		Port:         st.Server.Spec.Sms.Port,
		SoftVersion:  st.Server.Spec.Sms.SoftVersion,
		AccountSid:   string(st.Sender.Data["accountSid"]),
		AccountToken: string(st.Sender.Data["accountToken"]),
	}

	content, err := st.GetContent()
	if err != nil {
		Logger.Error(err, "Failed to get content for sms")
		return nil, err
	}
	templateID := aiopsv1beta1.DefaultSMSTemplateID
	if st.Server.Spec.Sms.TemplateId != "" {
		templateID = st.Server.Spec.Sms.TemplateId
	}
	body := &sms.Body{
		To:         "",
		AppId:      string(st.Sender.Data["appId"]),
		TemplateId: templateID,
		Datas:      []string{content},
	}
	return &adapter.SMSAdapter{
		SMS:       sms.NewSMS(sender, body),
		Receivers: st.Receivers,
	}, nil
}

func (st *NotificationSubTask) NewWebhookAdapter() (Adapter, error) {
	headers := map[string]string{}
	if st.Sender != nil {
		for k, v := range st.Sender.Data {
			headers[k] = string(v)
		}
	}
	var body interface{}
	if err := json.Unmarshal(st.Message.Spec.Body.Raw, &body); err != nil {
		Logger.Error(err, "Failed to unmarshal message body for webhook")
		return nil, err
	}
	return &adapter.WebhookAdapter{
		Webhook:   webhook.NewWebhook("", body, headers),
		Receivers: st.Receivers,
	}, nil
}

func (st *NotificationSubTask) NewDingtalkAdapter() (Adapter, error) {
	body, err := st.GetContent()
	if err != nil {
		Logger.Error(err, "Failed to get content for dingtalk")
		return nil, err
	}
	msgBody := Body{
		MsgType: "text",
		Text: Content{
			Content: body,
		},
	}
	return &adapter.DingtalkAdapter{
		Webhook:   webhook.NewWebhook("", msgBody, nil),
		Receivers: st.Receivers,
	}, nil
}

func (st *NotificationSubTask) NewWechatAdapter() (Adapter, error) {
	body, err := st.GetContent()
	if err != nil {
		Logger.Error(err, "Failed to get content for wechat")
		return nil, err
	}
	msgBody := Body{
		MsgType: "text",
		Text: Content{
			Content: body,
		},
	}
	return &adapter.WechatAdapter{
		Webhook:   webhook.NewWebhook("", msgBody, nil),
		Receivers: st.Receivers,
	}, nil
}
