package email

import (
	"crypto/tls"
	"fmt"

	"gopkg.in/gomail.v2"
	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
)

type ContentType string

const (
	HtmlContentType ContentType = "text/html"
	TextContentType ContentType = "text/plain"
)

var Logger = log.Log.WithName("email")

type Sender struct {
	Host               string `json:"host" required:"true"`
	Port               int    `json:"port" required:"true"`
	Username           string `json:"username" required:"true"`
	Password           string `json:"password" required:"true"`
	InsecureSkipVerify bool   `json:"insecure_skip_verify"`
	SSL                bool   `json:"ssl"`
}

type Content struct {
	Body string      `json:"body"`
	Type ContentType `json:"type"`
}

type Email struct {
	Sender    *Sender  `json:"sender"`
	Subject   string   `json:"subject"`
	Content   *Content `json:"content"`
	Receivers []string `json:"receivers"`
}

func NewEmail(sender *Sender, subject string, content *Content, receivers []string) *Email {
	return &Email{
		Sender:    sender,
		Subject:   subject,
		Content:   content,
		Receivers: receivers,
	}
}

func (e *Email) Validate() error {
	if len(e.Receivers) == 0 {
		Logger.Info("Email has no receivers")
		return fmt.Errorf("email has no receivers")
	}
	return nil
}

func (e *Email) Send() error {
	if err := e.Validate(); err != nil {
		Logger.Error(err, "Validate email error")
		return err
	}

	Logger.Info("Start send email", "from", e.Sender.Username, "to", e.Receivers)
	mail := gomail.NewMessage()
	mail.SetHeader("From", e.Sender.Username)
	mail.SetHeader("To", e.Receivers...)
	mail.SetHeader("Subject", e.Subject)
	mail.SetBody(string(e.Content.Type), e.Content.Body)
	Logger.V(2).Info("Email body", "subject", e.Subject, "content", e.Content.Body)

	d := &gomail.Dialer{
		Host:     e.Sender.Host,
		Port:     e.Sender.Port,
		Username: e.Sender.Username,
		Password: e.Sender.Password,
		SSL:      e.Sender.SSL,
	}
	if e.Sender.InsecureSkipVerify {
		d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	}
	if err := d.DialAndSend(mail); err != nil {
		Logger.Error(err, "Send email error")
		return err
	}
	Logger.Info("Email has been sent successfully", "from", e.Sender.Username, "to", e.Receivers)
	return nil
}
