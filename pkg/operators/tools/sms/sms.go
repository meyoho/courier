package sms

import (
	"bytes"
	"crypto/md5"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
)

var Logger = log.Log.WithName("sms")

var (
	smsURL = "%v:%v/%v/Accounts/%v/SMS/TemplateSMS?sig=%v"
)

type Body struct {
	To         string   `json:"to"`
	AppId      string   `json:"appId"`
	TemplateId string   `json:"templateId"`
	Datas      []string `json:"datas"`
}

type Sender struct {
	Host         string `json:"host"`
	Port         int32  `json:"port"`
	SoftVersion  string `json:"softversion"`
	AccountSid   string `json:"account_sid"`
	AccountToken string `json:"account_token"`
}

type StatusBody struct {
	StatusCode  string    `json:"statusCode"`
	StatusMsg   string    `json:"statusMsg"`
	TemplateSMS SMSResult `json:"templateSMS"`
}

type SMSResult struct {
	SmsMessageSid string `json:"smsMessageSid"`
	DateCreated   string `json:"dateCreated"`
}

type SMS struct {
	Sender *Sender `json:"sender"`
	Body   *Body   `json:"body"`
}

func NewSMS(sender *Sender, body *Body) *SMS {
	return &SMS{
		Sender: sender,
		Body:   body,
	}
}

func (s *SMS) Validate() error {
	if s.Body.To == "" {
		Logger.Info("SMS has no receivers")
		return fmt.Errorf("sms has no s.Body.To")
	}
	return nil
}

func (s *SMS) Send() error {
	if err := s.Validate(); err != nil {
		Logger.Error(err, "Validate sms error")
		return err
	}

	timeNow := time.Now().Format("20060102150405")
	sigParameter := md5.Sum([]byte(s.Sender.AccountSid + s.Sender.AccountToken + timeNow))
	url := fmt.Sprintf(smsURL,
		s.Sender.Host,
		s.Sender.Port,
		s.Sender.SoftVersion,
		s.Sender.AccountSid,
		strings.ToUpper(fmt.Sprintf("%x", sigParameter)),
	)
	Logger.Info("Start send sms", "from", url, "to", s.Body.To)

	// If content is too long, sms server will refuse the request
	// Cut off the content if length of content is more than 800
	for i, val := range s.Body.Datas {
		if len(val) > 800 {
			s.Body.Datas[i] = string([]rune(val)[:800]) + "..."
		}
	}

	bodyBytes, err := json.Marshal(s.Body)
	if err != nil {
		Logger.Error(err, "SMS marshal body error")
		return err
	}
	bodyBuf := bytes.NewBuffer(bodyBytes)
	Logger.Info("SMS body", "body", bodyBuf.String())

	req, err := http.NewRequest(http.MethodPost, url, bodyBuf)
	if err != nil {
		Logger.Error(err, "SMS new request err")
		return err
	}

	authData := base64.StdEncoding.EncodeToString([]byte(s.Sender.AccountSid + ":" + timeNow))
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json;charset=utf-8")
	req.Header.Add("Authorization", authData)
	Logger.Info("SMS request header", "header", req.Header)

	hc := &http.Client{Timeout: time.Second * 10}
	resp, err := hc.Do(req)
	if err != nil {
		Logger.Error(err, "SMS send error", "to", s.Body.To)
		return err
	}
	Logger.Info("SMS response status", "status", resp.Status, "code", resp.StatusCode)

	var statusBody StatusBody
	body, err := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(body, &statusBody)
	if err != nil {
		Logger.Error(err, "Unmarshal sms response body err")
		return err
	}
	Logger.Info("SMS response body", "body", statusBody)
	if statusBody.StatusCode != "000000" {
		Logger.Error(err, "SMS sent notification error")
		return fmt.Errorf("sms sent notification error: %+v", statusBody.StatusMsg)
	}
	resp.Body.Close()
	Logger.Info("SMS has been sent successfully", "to", s.Body.To)
	return nil
}
