package utils

import (
	"strconv"
	"time"
)

type FuncMap map[string]interface{}

var CustomFuncs = FuncMap{
	// dateFormat: turn a date string to layout format
	"dateFormat": func(date, layout string) string {
		t, _ := time.Parse(layout, date)
		return t.Format(layout)
	},
	// float: turn a numeric characters to float64
	"float": func(s string) float64 {
		f, _ := strconv.ParseFloat(s, 64)
		return f
	},
}
