package adapter

import (
	"fmt"
	"strings"

	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"
	"bitbucket.org/mathildetech/courier/pkg/operators/tools/email"
	"bitbucket.org/mathildetech/courier/pkg/operators/tools/sms"
	"bitbucket.org/mathildetech/courier/pkg/operators/tools/webhook"
)

const (
	StatusFailed    = "False"
	StatusSucceeded = "True"

	DingtalkURL = "https://oapi.dingtalk.com/robot/send?access_token=%v"
	WechatURL   = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=%v"
)

type Adapter interface {
	Send() ([]*SenderResult, error)
}

type SenderResult struct {
	Receiver *aiopsv1beta1.NotificationReceiver
	Status   aiopsv1beta1.ConditionStatus
	Message  string
	Reason   string
}

type EmailAdapter struct {
	Email     *email.Email
	Receivers []*aiopsv1beta1.NotificationReceiver
}

type SMSAdapter struct {
	SMS       *sms.SMS
	Receivers []*aiopsv1beta1.NotificationReceiver
}

type WebhookAdapter struct {
	Webhook   *webhook.Webhook
	Receivers []*aiopsv1beta1.NotificationReceiver
}

type DingtalkAdapter struct {
	Webhook   *webhook.Webhook
	Receivers []*aiopsv1beta1.NotificationReceiver
}

type WechatAdapter struct {
	Webhook   *webhook.Webhook
	Receivers []*aiopsv1beta1.NotificationReceiver
}

func (e *EmailAdapter) Send() ([]*SenderResult, error) {
	var receivers []string
	for _, r := range e.Receivers {
		receivers = append(receivers, r.Spec.Destination)
	}
	e.Email.Receivers = receivers
	err := e.Email.Send()
	result := make([]*SenderResult, len(e.Receivers))
	for i, _ := range e.Receivers {
		result[i] = &SenderResult{}
		result[i].Receiver = e.Receivers[i]
		if err != nil {
			result[i].Status = StatusFailed
			result[i].Message = err.Error()
		} else {
			result[i].Status = StatusSucceeded
		}
	}
	return result, err
}

func (s *SMSAdapter) Send() ([]*SenderResult, error) {
	var err error
	result := make([]*SenderResult, len(s.Receivers))
	for i, v := range s.Receivers {
		result[i] = &SenderResult{}
		result[i].Receiver = s.Receivers[i]
		s.SMS.Body.To = v.Spec.Destination
		if err = s.SMS.Send(); err != nil {
			result[i].Status = StatusFailed
			result[i].Message = err.Error()
		} else {
			result[i].Status = StatusSucceeded
		}
	}
	return result, err
}

func (w *WebhookAdapter) Send() ([]*SenderResult, error) {
	var err error
	result := make([]*SenderResult, len(w.Receivers))
	for i, v := range w.Receivers {
		result[i] = &SenderResult{}
		result[i].Receiver = w.Receivers[i]
		// Update adapter webhook's url
		if err := w.Webhook.UpdateUrl(v.Spec.Destination); err != nil {
			result[i].Status = StatusFailed
			result[i].Message = err.Error()
			continue
		}
		// Send the webhook
		if err = w.Webhook.Send(); err != nil {
			result[i].Status = StatusFailed
			result[i].Message = err.Error()
		} else {
			result[i].Status = StatusSucceeded
		}
	}
	return result, err
}

func (d *DingtalkAdapter) Send() ([]*SenderResult, error) {
	var err error
	result := make([]*SenderResult, len(d.Receivers))
	for i, v := range d.Receivers {
		result[i] = &SenderResult{}
		result[i].Receiver = d.Receivers[i]
		// Update adapter webhook's url
		destination := ""
		if strings.HasPrefix(v.Spec.Destination, "https://") {
			destination = v.Spec.Destination
		} else {
			destination = fmt.Sprintf(DingtalkURL, v.Spec.Destination)
		}
		if err := d.Webhook.UpdateUrl(destination); err != nil {
			result[i].Status = StatusFailed
			result[i].Message = err.Error()
			continue
		}
		// Send the webhook
		if err = d.Webhook.Send(); err != nil {
			result[i].Status = StatusFailed
			result[i].Message = err.Error()
		} else {
			result[i].Status = StatusSucceeded
		}
	}
	return result, err
}

func (w *WechatAdapter) Send() ([]*SenderResult, error) {
	var err error
	result := make([]*SenderResult, len(w.Receivers))
	for i, v := range w.Receivers {
		result[i] = &SenderResult{}
		result[i].Receiver = w.Receivers[i]
		// Update adapter webhook's url
		destination := ""
		if strings.HasPrefix(v.Spec.Destination, "https://") {
			destination = v.Spec.Destination
		} else {
			destination = fmt.Sprintf(DingtalkURL, v.Spec.Destination)
		}
		if err := w.Webhook.UpdateUrl(destination); err != nil {
			result[i].Status = StatusFailed
			result[i].Message = err.Error()
			continue
		}
		// Send the webhook
		if err = w.Webhook.Send(); err != nil {
			result[i].Status = StatusFailed
			result[i].Message = err.Error()
		} else {
			result[i].Status = StatusSucceeded
		}
	}
	return result, err
}
