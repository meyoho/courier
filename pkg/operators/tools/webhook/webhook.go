package webhook

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
)

const (
	mediaType = "application/json"
	userAgent = "Alauda/Courier"
)

var Logger = log.Log.WithName("webhook")

type Webhook struct {
	Request *http.Request     `json:"request"`
	URL     string            `json:"url"`
	Method  string            `json:"method"`
	Body    interface{}       `json:"body"`
	Headers map[string]string `json:"headers"`
}

func NewWebhook(url string, body interface{}, headers map[string]string) *Webhook {
	bodyBytes, err := json.Marshal(body)
	if err != nil {
		Logger.Error(err, "Marshal webhook body error")
		return nil
	}
	bodyReader := bytes.NewReader(bodyBytes)
	req, err := http.NewRequest(http.MethodPost, url, bodyReader)
	if err != nil {
		Logger.Error(err, "New request for webhook error")
		return nil
	}
	req.Header.Add("Content-Type", mediaType)
	req.Header.Add("Accept", mediaType)
	req.Header.Add("User-Agent", userAgent)
	for k, v := range headers {
		req.Header.Add(k, v)
	}

	return &Webhook{
		Request: req,
		URL:     url,
		Method:  http.MethodPost,
		Body:    body,
		Headers: headers,
	}
}

func (w *Webhook) Validate() error {
	if w.URL == "" {
		Logger.Info("Webhook has no receivers")
		return fmt.Errorf("Webhook has no receivers")
	}
	return nil
}

func (w *Webhook) Send() error {
	if err := w.Validate(); err != nil {
		Logger.Error(err, "Validate webhook error")
		return err
	}

	Logger.Info("Start send webhook ", "to", w.URL)
	Logger.Info("Webhook body", "body", w.Body)
	hc := http.Client{Timeout: time.Second * 10}
	resp, err := hc.Do(w.Request)
	if err != nil {
		Logger.Error(err, "Failed to send webhook", "to", w.URL)
		return err
	}
	resp.Body.Close()
	Logger.Info("Webhook has been sent successfully", "to", w.URL)
	return nil
}

func (w *Webhook) UpdateUrl(rawUrl string) error {
	u, err := url.Parse(rawUrl)
	if err != nil {
		return err
	}
	w.URL = rawUrl
	w.Request.URL = u
	w.Request.Host = u.Host
	return nil
}
