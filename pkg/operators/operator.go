package operators

import (
	"fmt"

	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"
	"bitbucket.org/mathildetech/courier/pkg/operators/tools/adapter"

	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
)

var Logger = log.Log.WithName("operator")

func Send(task *NotificationTask) ([]*adapter.SenderResult, error) {
	subTasks := generateNotificationSubTask(task)

	var senderResult []*adapter.SenderResult
	for _, st := range subTasks {
		if st.HasDone {
			continue
		}
		// Validate subscription configuration
		if err := st.Validate(); err != nil {
			Logger.Error(err, "Validate subscription failed")
			return nil, err
		}
		// Generate adapter for this subtask
		adapter, err := st.GetAdapter()
		if err != nil {
			Logger.Error(err, "Get adapter failed")
			return nil, err
		}
		// Try to send message to a subscription
		sr, err := adapter.Send()
		if err != nil {
			Logger.Error(err, "Send adapter failed")
		}
		st.SenderResult = sr
		senderResult = append(senderResult, sr...)
	}
	return senderResult, nil
}

func (st *NotificationSubTask) GetAdapter() (Adapter, error) {
	if err := st.Validate(); err != nil {
		return nil, err
	}
	switch st.Method {
	case aiopsv1beta1.MethodEmail:
		return st.NewEmailAdapter()
	case aiopsv1beta1.MethodSMS:
		return st.NewSMSAdapter()
	case aiopsv1beta1.MethodWebhook:
		return st.NewWebhookAdapter()
	case aiopsv1beta1.MethodDingtalk:
		return st.NewDingtalkAdapter()
	case aiopsv1beta1.MethodWechat:
		return st.NewWechatAdapter()
	default:
		Logger.Info("Unsupported method " + st.Method + " , skip")
		return nil, fmt.Errorf("method %s unsupported yet", st.Method)
	}
}

func (st *NotificationSubTask) Validate() error {
	// Template is a required for both methods
	if st.Template == nil {
		return fmt.Errorf("can not find template for notification %s, method %s", st.Notification.Name, st.Method)
	}

	// Receivers can not be empty list
	if len(st.Receivers) == 0 {
		return fmt.Errorf("receivers list can not be empty for notification %s, method %s", st.Notification.Name, st.Method)
	}

	// Sender and Server is required for email and sms
	if st.Method == aiopsv1beta1.MethodEmail || st.Method == aiopsv1beta1.MethodSMS {
		if st.Sender == nil {
			return fmt.Errorf("can not find sender for notification %s, method %s", st.Notification.Name, st.Method)
		}
		if st.Server == nil {
			return fmt.Errorf("can not find server for notification %s, method %s", st.Notification.Name, st.Method)
		}
	}
	return nil
}

// generateNotificationSubTask will generate subtasks with one receiver for a task.
func generateNotificationSubTask(task *NotificationTask) []*NotificationSubTask {
	var subTasks []*NotificationSubTask
	for _, s := range task.Notification.Spec.Subscriptions {
		receivers := task.GetReceivers(s)
		for _, r := range receivers {
			st := &NotificationSubTask{
				Method:       s.Method,
				Message:      task.Message,
				Notification: task.Notification,
				Template:     task.GetTemplate(s),
				Sender:       task.GetSender(s),
				Server:       task.GetServer(s),
				Receivers:    []*aiopsv1beta1.NotificationReceiver{r},
				HasDone:      false,
			}
			for _, c := range task.Message.Status.Conditions {
				if c.Notification == task.Notification.Name && c.Method == st.Method && c.ReceiverNamespace == r.Namespace && c.ReceiverName == r.Name {
					if c.Status == aiopsv1beta1.ConditionTrue || (c.Status == aiopsv1beta1.ConditionFalse && c.RetryTimes >= 5) {
						st.HasDone = true
						break
					}
				}
			}
			subTasks = append(subTasks, st)
		}
	}
	return subTasks
}
