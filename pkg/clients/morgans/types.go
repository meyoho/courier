package morgans

import (
	"fmt"
	"time"
)

type MeterSummaryResponse struct {
	MeterList `json:",inline"`
	Results   []*MeterSummaryItem `json:"results"`
}

type MeterSummaryItem struct {
	MeterItem `json:",inline"`
}

type MeterQueryResponse struct {
	MeterList `json:",inline"`
	Results   []*MeterQueryItem `json:"results"`
}

type MeterQueryItem struct {
	MeterItem `json:",inline"`
	Pod       string `json:"pod,omitempty"`
}

type MeterList struct {
	Count    int64  `json:"count"`
	PageSize int64  `json:"page_size"`
	Page     int    `json:"num_pages"`
	Next     string `json:"next"`
	Previous string `json:"previous"`
}

type MeterItem struct {
	Project   NameRecord  `json:"project,omitempty"`
	Cluster   NameRecord  `json:"cluster,omitempty"`
	Namespace NameRecord  `json:"namespace,omitempty"`
	Meter     MeterRecord `json:"meter"`
	StartTime time.Time   `json:"startTime"`
	EndTime   time.Time   `json:"endTime"`
}

type NameRecord struct {
	Name        string `json:"name"`
	DisplayName string `json:"displayName,omitempty"`
}

type MeterRecord struct {
	CPU    float64 `json:"cpu"`
	Memory float64 `json:"memory"`
}

func (mr *MeterRecord) Add(o *MeterRecord) {
	mr.CPU += o.CPU
	mr.Memory += o.Memory
}

func (mr *MeterRecord) GetCPU() string {
	return fmt.Sprintf("%.2f", mr.CPU)
}

func (mr *MeterRecord) GetMemory() string {
	return fmt.Sprintf("%.2f", mr.Memory)
}
