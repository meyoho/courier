package morgans

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"os"
	"path/filepath"
	"time"

	"github.com/levigross/grequests"
	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
)

var (
	Client *Component
	Logger = log.Log.WithName("morgans_client")
)

type Component struct {
	Endpoint   string
	ApiVersion string
	Timeout    int
	Token      string
}

func (c *Component) GetSummary(params map[string]string) ([]*MeterSummaryItem, bool, error) {
	url := fmt.Sprintf("%s/%s/meter/summary", c.Endpoint, c.ApiVersion)
	begin := time.Now()
	Logger.Info(fmt.Sprintf("Request morgans: GET %s, params %v", url, params))
	response, err := grequests.Get(url, &grequests.RequestOptions{
		RequestTimeout: time.Duration(c.Timeout) * time.Second,
		Params:         params,
		Headers:        map[string]string{"Authorization": "Bearer " + c.Token},
	})
	if err != nil {
		Logger.Error(err, "Request morgans error")
		return nil, false, err
	}
	Logger.Info(fmt.Sprintf("Request morgans: Code %d, Latency %.6fs", response.StatusCode, time.Since(begin).Seconds()))
	if response.StatusCode >= 300 {
		Logger.Info(fmt.Sprintf("Code %d, Response %s", response.StatusCode, response.String()))
		return nil, false, fmt.Errorf("morgans response code (%d) is more than 300", response.StatusCode)
	}

	result := MeterSummaryResponse{}
	if err := json.Unmarshal([]byte(response.String()), &result); err != nil {
		Logger.Error(err, "Unmarshal response error")
		return nil, false, err
	}
	return result.Results, result.Next != "", nil
}

func (c *Component) GetQuery(params map[string]string) ([]*MeterQueryItem, bool, error) {
	url := fmt.Sprintf("%s/%s/meter/query", c.Endpoint, c.ApiVersion)
	begin := time.Now()
	Logger.Info(fmt.Sprintf("Request morgans: GET %s, params %v", url, params))
	response, err := grequests.Get(url, &grequests.RequestOptions{
		RequestTimeout: time.Duration(c.Timeout) * time.Second,
		Params:         params,
		Headers:        map[string]string{"Authorization": "Bearer " + c.Token},
	})
	if err != nil {
		Logger.Error(err, "Request morgans error")
		return nil, false, err
	}
	Logger.Info(fmt.Sprintf("Request morgans: Code %d, Latency %.6fs", response.StatusCode, time.Since(begin).Seconds()))
	if response.StatusCode >= 300 {
		Logger.Info(fmt.Sprintf("Code %d, Response %s", response.StatusCode, response.String()))
		return nil, false, fmt.Errorf("morgans response code (%d) is more than 300", response.StatusCode)
	}

	result := MeterQueryResponse{}
	if err := json.Unmarshal([]byte(response.String()), &result); err != nil {
		Logger.Error(err, "Unmarshal response error")
		return nil, false, err
	}
	return result.Results, result.Next != "", nil
}

func (c *Component) UploadRequest(name, path string) error {
	url := fmt.Sprintf("%s/%s/meter/reports", c.Endpoint, c.ApiVersion)

	// Open file, initialize multipart and copy file content to multipart.
	fp, err := os.Open(path)
	if err != nil {
		return err
	}
	defer fp.Close()
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(name, filepath.Base(path))
	if err != nil {
		return err
	}
	if _, err = io.Copy(part, fp); err != nil {
		return err
	}
	if err = writer.Close(); err != nil {
		return err
	}
	headers := map[string]string{"Authorization": "Bearer " + c.Token, "Content-Type": "multipart/form-data; boundary=" + writer.Boundary()}
	// Upload file with a post request.
	begin := time.Now()
	Logger.Info(fmt.Sprintf("Request morgans: POST %s, upload file %s", url, name))
	response, err := grequests.Post(url, &grequests.RequestOptions{RequestTimeout: time.Duration(c.Timeout) * time.Second, Headers: headers, RequestBody: body})
	if err != nil {
		Logger.Error(err, "Request morgans error")
		return err
	}
	Logger.Info(fmt.Sprintf("Request morgans: Code %d, Latency %.6fs", response.StatusCode, time.Since(begin).Seconds()))
	if response.StatusCode >= 300 {
		Logger.Info(fmt.Sprintf("Code %d, Response %s", response.StatusCode, response.String()))
		return fmt.Errorf("morgans response code (%d) is more than 300", response.StatusCode)
	}
	return nil
}

func Init(endpoint string, apiVersion string, timeout int) {
	Client = &Component{
		Endpoint:   endpoint,
		ApiVersion: apiVersion,
		Timeout:    timeout,
	}

	var tokenFile = "/var/run/secrets/kubernetes.io/serviceaccount/token"
	if token, err := ioutil.ReadFile(tokenFile); err == nil {
		Client.Token = string(token)
	} else {
		Logger.Error(err, "Read token from file error")
	}
}
