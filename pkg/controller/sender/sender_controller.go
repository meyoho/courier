/*
Copyright 2019 Alauda AIOps Team.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package sender

import (
	"context"
	"encoding/json"
	"time"

	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var Logger = log.Log.WithName("sender-controller")

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new NotificationSender Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileSender{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("sender-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to NotificationSender
	err = c.Watch(&source.Kind{Type: &aiopsv1beta1.NotificationSender{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}
	return nil
}

var _ reconcile.Reconciler = &ReconcileSender{}

// ReconcileMessage reconciles a NotificationSender object
type ReconcileSender struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a NotificationSender object and makes changes based on the state read
// and what is in the NotificationSender.Spec
// +kubebuilder:rbac:groups=aiops.alauda.io,resources=messages,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=aiops.alauda.io,resources=messages/status,verbs=get;update;patch
func (r *ReconcileSender) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the NotificationSender instance
	instance := &aiopsv1beta1.NotificationSender{}
	if err := r.Get(context.TODO(), request.NamespacedName, instance); err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}
	Logger.Info("Step into notification sender reconcile", "sender", instance.Name)
	if HasSynced(instance) {
		// If notification sender has been synced, skip
		return reconcile.Result{}, nil
	}

	// Generate a sender parser to parse secret and server
	parser, err := GenerateNotificationSenderParser(r, instance)
	if err != nil {
		Logger.Error(err, "Generate notification sender parser error", "sender", instance.Name)
		return r.UpdateSyncResult(instance, err)
	}
	secret := parser.ParseSecret()
	server := parser.ParseServer()

	// Create or update notification sender secret
	if parser.Secret == nil {
		err = r.Create(context.Background(), secret)
	} else {
		err = r.Update(context.Background(), secret)
	}
	if err != nil {
		Logger.Error(err, "Apply notification sender secret error", "sender", instance.Name)
		return r.UpdateSyncResult(instance, err)
	}
	if server == nil {
		Logger.Info("Webhook sender has no notification server, skip", "sender", instance.Name)
		return r.UpdateSyncResult(instance, nil)
	}
	//  Create or update notification server
	if parser.Server == nil {
		err = r.Create(context.Background(), server)
	} else {
		err = r.Update(context.Background(), server)
	}
	if err != nil {
		Logger.Error(err, "Apply notification server error", "sender", instance.Name)
	}
	Logger.Info("Step out notification sender reconcile", "sender", instance.Name)
	return r.UpdateSyncResult(instance, err)
}

func (r *ReconcileSender) UpdateSyncResult(ns *aiopsv1beta1.NotificationSender, err error) (reconcile.Result, error) {
	resultInstance := &SynchronizationResult{}
	if err != nil {
		resultInstance.Status = StatusFailed
		resultInstance.Reason = err.Error()
	} else {
		resultInstance.Status = StatusSucceeded
	}
	bytes, err := json.Marshal(*resultInstance)
	if err != nil {
		return reconcile.Result{}, nil
	}
	if ns.Annotations == nil {
		ns.Annotations = map[string]string{}
	}
	ns.Annotations[aiopsv1beta1.AnnotationKeySynchronizationResult] = string(bytes)
	if err = r.Update(context.Background(), ns); err != nil {
		Logger.Error(err, "Update sync result error", "sender", ns.Name)
		after, _ := time.ParseDuration("60s")
		return reconcile.Result{RequeueAfter: after}, nil
	}
	return reconcile.Result{}, nil
}
