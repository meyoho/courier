package sender

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"

	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"

	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

const (
	StatusFailed    = aiopsv1beta1.ConditionFalse
	StatusSucceeded = aiopsv1beta1.ConditionTrue
)

type SynchronizationResult struct {
	Status aiopsv1beta1.ConditionStatus `json:"status"`
	Reason string                       `json:"reason,omitempty"`
}

func HasSynced(ns *aiopsv1beta1.NotificationSender) bool {
	result, ok := ns.Annotations[aiopsv1beta1.AnnotationKeySynchronizationResult]
	if !ok {
		return false
	}
	resultInstance := &SynchronizationResult{}
	if err := json.Unmarshal([]byte(result), resultInstance); err != nil {
		return false
	}
	return resultInstance.Status == StatusSucceeded
}

type NotificationSenderParser struct {
	Method string
	Secret *v1.Secret
	Server *aiopsv1beta1.NotificationServer
	Sender *aiopsv1beta1.NotificationSender
}

func (n *NotificationSenderParser) ParseSecret() *v1.Secret {
	secret := n.Secret
	if secret == nil {
		secret = &v1.Secret{
			Type:     aiopsv1beta1.SecretTypeNotificationSender,
			TypeMeta: metav1.TypeMeta{APIVersion: "v1", Kind: "Secret"},
			ObjectMeta: metav1.ObjectMeta{
				Name:      n.Sender.Name,
				Namespace: aiopsv1beta1.ControllerNamespace,
				Labels: map[string]string{
					aiopsv1beta1.LabelKeyType:   n.Method,
					aiopsv1beta1.LabelKeyServer: n.Sender.Name,
				},
			},
			Data: map[string][]byte{},
		}
	}
	if secret.Data == nil {
		secret.Data = map[string][]byte{}
	}
	switch n.Method {
	case aiopsv1beta1.MethodEmail:
		secret.Data["username"] = []byte(n.Sender.Spec.Email.Username)
		secret.Data["password"] = []byte(n.Sender.Spec.Email.Password.Value)
	case aiopsv1beta1.MethodSMS:
		secret.Data["appId"] = []byte(n.Sender.Spec.Sms.AppId)
		secret.Data["accountSid"] = []byte(n.Sender.Spec.Sms.AccountSid.Value)
		secret.Data["accountToken"] = []byte(n.Sender.Spec.Sms.AccountToken.Value)
	case aiopsv1beta1.MethodWebhook:
		for _, i := range n.Sender.Spec.Webhook.Headers {
			secret.Data[i.Key] = []byte(i.Value.Value)
		}
		username := n.Sender.Spec.Webhook.Auth.Basic.Username
		password := n.Sender.Spec.Webhook.Auth.Basic.Password.Value
		if username != "" && password != "" {
			secret.Data["Authorization"] = []byte("Basic " + base64.StdEncoding.EncodeToString([]byte(username+":"+password)))
		}
		delete(secret.Labels, aiopsv1beta1.LabelKeyServer)
	}
	return secret
}

func (n *NotificationSenderParser) ParseServer() *aiopsv1beta1.NotificationServer {
	server := n.Server
	if server == nil {
		server = &aiopsv1beta1.NotificationServer{
			TypeMeta: metav1.TypeMeta{APIVersion: aiopsv1beta1.SchemeGroupVersion.String(), Kind: "NotificationServer"},
			ObjectMeta: metav1.ObjectMeta{
				Name:      n.Sender.Name,
				Namespace: aiopsv1beta1.ControllerNamespace,
				Labels: map[string]string{
					aiopsv1beta1.LabelKeyType: n.Method,
				},
			},
			Spec: aiopsv1beta1.NotificationServerSpec{
				Email: &aiopsv1beta1.EmailServer{},
				Sms:   &aiopsv1beta1.SmsServer{},
			},
		}
	}
	switch n.Method {
	case aiopsv1beta1.MethodEmail:
		server.Spec.Sms = nil
		server.Spec.Email.Host = n.Sender.Spec.Email.Host
		server.Spec.Email.Port = n.Sender.Spec.Email.Port
		server.Spec.Email.SslEnabled = n.Sender.Spec.Email.SslEnabled
		server.Spec.Email.InsecureSkipVerify = n.Sender.Spec.Email.InsecureSkipVerify
	case aiopsv1beta1.MethodSMS:
		server.Spec.Email = nil
		server.Spec.Sms.Host = n.Sender.Spec.Sms.Host
		server.Spec.Sms.Port = n.Sender.Spec.Sms.Port
		server.Spec.Sms.SoftVersion = n.Sender.Spec.Sms.SoftVersion
		server.Spec.Sms.TemplateId = n.Sender.Spec.Sms.TemplateId
	case aiopsv1beta1.MethodWebhook:
		return nil
	}
	return server
}

func (n *NotificationSenderParser) Validate() error {
	if n.Method != aiopsv1beta1.MethodEmail && n.Method != aiopsv1beta1.MethodSMS && n.Method != aiopsv1beta1.MethodWebhook {
		return fmt.Errorf("unsupported method %s", n.Method)
	}
	if n.Secret != nil {
		if n.Secret.Type != aiopsv1beta1.SecretTypeNotificationSender {
			return fmt.Errorf("secret %s/%s already exists, but type is %s(should be %s)", n.Secret.Namespace, n.Secret.Name, n.Secret.Type, aiopsv1beta1.SecretTypeNotificationSender)
		}
		if n.Secret.Labels[aiopsv1beta1.LabelKeyType] != n.Method {
			return fmt.Errorf("secret %s/%s already exists, but type is %s(should be %s)", n.Secret.Namespace, n.Secret.Name, n.Secret.Labels[aiopsv1beta1.LabelKeyType], n.Method)
		}
	}
	if n.Server != nil {
		if n.Secret.Labels[aiopsv1beta1.LabelKeyType] != n.Method && n.Method != aiopsv1beta1.MethodWebhook {
			return fmt.Errorf("server %s already exists, but type is %s(should be %s)", n.Server.Name, n.Server.Labels[aiopsv1beta1.LabelKeyType], n.Method)
		}
	}
	return nil
}

// GenerateNotificationSenderParser will generate a notification sender parser with all referred resources.
func GenerateNotificationSenderParser(rm *ReconcileSender, ns *aiopsv1beta1.NotificationSender) (*NotificationSenderParser, error) {
	parser := &NotificationSenderParser{
		Server: &aiopsv1beta1.NotificationServer{},
		Secret: &v1.Secret{},
		Sender: ns,
	}

	if err := rm.Get(context.TODO(), types.NamespacedName{Namespace: aiopsv1beta1.ControllerNamespace, Name: ns.Name}, parser.Secret); err != nil {
		if !errors.IsNotFound(err) {
			return nil, err
		}
		parser.Secret = nil
	}

	if err := rm.Get(context.TODO(), types.NamespacedName{Name: ns.Name}, parser.Server); err != nil {
		if !errors.IsNotFound(err) {
			return nil, err
		}
		parser.Server = nil
	}

	parser.Method = ns.Labels[aiopsv1beta1.LabelKeyType]
	if parser.Method == "" {
		if ns.Spec.Email != nil && ns.Spec.Email.Host != "" {
			parser.Method = aiopsv1beta1.MethodEmail
		} else if ns.Spec.Sms != nil && ns.Spec.Sms.Host != "" {
			parser.Method = aiopsv1beta1.MethodSMS
		} else if ns.Spec.Webhook != nil && len(ns.Spec.Webhook.Headers) > 0 {
			parser.Method = aiopsv1beta1.MethodWebhook
		}
	}
	if err := addValueFromSecretForSender(rm, parser.Method, ns); err != nil {
		return nil, err
	}
	return parser, parser.Validate()
}

func addValueFromSecretForSender(rm *ReconcileSender, method string, ns *aiopsv1beta1.NotificationSender) error {
	parseSecretValue := func(vsp *aiopsv1beta1.ValueWithSecret) error {
		if vsp.Value != "" {
			return nil
		}
		if vsp.ValueFromSecret.Name == "" {
			return nil
		}
		vs := &v1.Secret{}
		if err := rm.Get(context.TODO(), types.NamespacedName{Name: vsp.ValueFromSecret.Name, Namespace: vsp.ValueFromSecret.Namespace}, vs); err != nil {
			return err
		}
		vsp.Value = string(vs.Data[vsp.ValueFromSecret.Key])
		return nil
	}

	switch method {
	case aiopsv1beta1.MethodEmail:
		ns.Spec.Sms = nil
		ns.Spec.Webhook = nil
		if err := parseSecretValue(&ns.Spec.Email.Password); err != nil {
			return err
		}
	case aiopsv1beta1.MethodSMS:
		ns.Spec.Email = nil
		ns.Spec.Webhook = nil
		if err := parseSecretValue(&ns.Spec.Sms.AccountToken); err != nil {
			return err
		}
		if err := parseSecretValue(&ns.Spec.Sms.AccountSid); err != nil {
			return err
		}
	case aiopsv1beta1.MethodWebhook:
		ns.Spec.Email = nil
		ns.Spec.Sms = nil
		if err := parseSecretValue(&ns.Spec.Webhook.Auth.Basic.Password); err != nil {
			return err
		}
		for _, i := range ns.Spec.Webhook.Headers {
			if err := parseSecretValue(&i.Value); err != nil {
				return err
			}
		}
	}
	return nil
}
