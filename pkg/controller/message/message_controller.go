/*
Copyright 2019 Alauda AIOps Team.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package message

import (
	"context"
	"time"

	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"
	"bitbucket.org/mathildetech/courier/pkg/operators"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var Logger = log.Log.WithName("controller")

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new NotificationMessage Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileMessage{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("message-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to NotificationMessage
	err = c.Watch(&source.Kind{Type: &aiopsv1beta1.NotificationMessage{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	//// TODO(user): Modify this to be the types you create
	//// Uncomment watch a Deployment created by NotificationMessage - change this for objects you create
	//err = c.Watch(&source.Kind{Type: &aiopsv1beta1.NotificationSender{}}, &handler.EnqueueRequestForObject{})
	//if err != nil {
	//	return err
	//}

	return nil
}

var _ reconcile.Reconciler = &ReconcileMessage{}

// ReconcileMessage reconciles a NotificationMessage object
type ReconcileMessage struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a NotificationMessage object and makes changes based on the state read
// and what is in the NotificationMessage.Spec
// TODO(user): Modify this Reconcile function to implement your Controller logic.  The scaffolding writes
// a Deployment as an example
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=aiops.alauda.io,resources=messages,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=aiops.alauda.io,resources=messages/status,verbs=get;update;patch
func (r *ReconcileMessage) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the NotificationMessage instance
	instance := &aiopsv1beta1.NotificationMessage{}
	if err := r.Get(context.TODO(), request.NamespacedName, instance); err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}
	Logger.Info("Step into notification message reconcile", "message", instance.Namespace+"/"+instance.Name)

	// Check if the message has been handled
	if IsReconciled(r, instance) {
		Logger.Info("This message has been handled, skip", "message", instance.Namespace+"/"+instance.Name)
		return reconcile.Result{}, nil
	}
	// Check if this message should be handled by courier, support custom controller
	ok, err := IsMyTurn(r, instance)
	if err != nil {
		Logger.Error(err, "Check if my turn error", "message", instance.Namespace+"/"+instance.Name)
		after, _ := time.ParseDuration("60s")
		return reconcile.Result{RequeueAfter: after}, nil
	}
	if !ok {
		Logger.Info("This message should not be handled by me, skip ", "message", instance.Namespace+"/"+instance.Name)
		return reconcile.Result{}, nil
	}

	// Send notification by operator
	var task *operators.NotificationTask
	for _, n := range instance.Spec.Notifications {
		conditions := []aiopsv1beta1.NotificationMessageCondition{}
		// Generate a task for each associated notification
		task, err = GenerateNotificationTask(r, instance, n.Name)
		if err != nil {
			// If error occurred, break. Usually caused by referred objects not found.
			Logger.Error(err, "Generate notification task failed", "message", instance.Namespace+"/"+instance.Name)
			conditions = append(conditions, aiopsv1beta1.NotificationMessageCondition{
				LastTransitionTime: metav1.Now(),
				Type:               aiopsv1beta1.MessageReady,
				Status:             aiopsv1beta1.ConditionFalse,
				Reason:             err.Error(),
			})
			return UpdateMessageStatus(r, instance, conditions)
		}
		// Use operator to send this message, and accept sender result to update message's status
		senderResults, err := operators.Send(task)
		if err != nil {
			// If error occurred, break. Usually caused by referred objects not found.
			Logger.Error(err, "Send notification task failed", "message", instance.Namespace+"/"+instance.Name)
			conditions = append(conditions, aiopsv1beta1.NotificationMessageCondition{
				LastTransitionTime: metav1.Now(),
				Type:               aiopsv1beta1.MessageReady,
				Status:             aiopsv1beta1.ConditionFalse,
				Reason:             err.Error(),
			})
			return UpdateMessageStatus(r, instance, conditions)
		}
		for _, sr := range senderResults {
			conditions = append(conditions, aiopsv1beta1.NotificationMessageCondition{
				Notification:       n.Name,
				Method:             sr.Receiver.Labels[aiopsv1beta1.LabelKeyType],
				ReceiverNamespace:  sr.Receiver.Namespace,
				ReceiverName:       sr.Receiver.Name,
				LastTransitionTime: metav1.Now(),
				Type:               aiopsv1beta1.MessageReady,
				Status:             sr.Status,
				Reason:             sr.Message,
			})
		}
		UpdateMessageStatus(r, instance, conditions)
	}
	Logger.Info("Step out notification message reconcile", "message", instance.Namespace+"/"+instance.Name)
	return reconcile.Result{}, nil
}
