package message

import (
	"context"
	"strings"
	"time"

	aiopsv1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"
	"bitbucket.org/mathildetech/courier/pkg/operators"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

// IsMyTurn will check whether this message should be handled by courier.
func IsMyTurn(rm *ReconcileMessage, nm *aiopsv1beta1.NotificationMessage) (bool, error) {
	for _, n := range nm.Spec.Notifications {
		notification := &aiopsv1beta1.Notification{}
		// Get associated notification for this task
		if err := rm.Get(context.TODO(), types.NamespacedName{Name: n.Name, Namespace: nm.Namespace}, notification); err != nil {
			if !errors.IsNotFound(err) {
				return false, err
			}
			continue
		}
		owner := strings.ToLower(notification.Labels[aiopsv1beta1.LabelKeyOwner])
		if owner != "" && owner != "system" && owner != "courier" && owner != "alauda" {
			return false, nil
		}
	}
	return true, nil
}

// IsReconciled will check whether this message has been processed.
func IsReconciled(rm *ReconcileMessage, nm *aiopsv1beta1.NotificationMessage) bool {
	if nm.Status.Conditions == nil {
		return false
	}
	for _, c := range nm.Status.Conditions {
		if c.Status == aiopsv1beta1.ConditionFalse && c.RetryTimes < 5 {
			return false
		}
	}
	return true
}

// UpdateMessageStatus will update status of notifition message
func UpdateMessageStatus(rm *ReconcileMessage, nm *aiopsv1beta1.NotificationMessage, conditions []aiopsv1beta1.NotificationMessageCondition) (reconcile.Result, error) {
	setMessageReadyCondition(nm, conditions)
	if err := rm.Status().Update(context.Background(), nm); err != nil {
		Logger.Error(err, "Update status error", "message", nm.Namespace+"/"+nm.Name)
		after, _ := time.ParseDuration("10s")
		Logger.Info("Step out notification message reconcile", "message", nm.Namespace+"/"+nm.Name)
		return reconcile.Result{RequeueAfter: after}, nil
	}
	Logger.Info("Step out notification message reconcile", "message", nm.Namespace+"/"+nm.Name)
	return reconcile.Result{}, nil
}

// setMessageReadyCondition will insert ready condition for this message.
func setMessageReadyCondition(nm *aiopsv1beta1.NotificationMessage, conditions []aiopsv1beta1.NotificationMessageCondition) {
	updateCondition := func(nm *aiopsv1beta1.NotificationMessage, condition aiopsv1beta1.NotificationMessageCondition) bool {
		for i, c := range nm.Status.Conditions {
			if c.Status == aiopsv1beta1.ConditionFalse && c.Notification == condition.Notification && c.Method == condition.Method && c.ReceiverNamespace == condition.ReceiverNamespace && c.ReceiverName == condition.ReceiverName {
				condition.RetryTimes = c.RetryTimes + 1
				nm.Status.Conditions[i] = condition
				return true
			}
		}
		return false
	}

	if nm.Status.Conditions == nil || len(nm.Status.Conditions) == 0 {
		nm.Status.Conditions = conditions
		return
	}
	for _, c := range conditions {
		if !updateCondition(nm, c) {
			nm.Status.Conditions = append(nm.Status.Conditions, c)
		}
	}
}

// GenerateNotificationTask will generate a notification task with all referred resources.
func GenerateNotificationTask(rm *ReconcileMessage, nm *aiopsv1beta1.NotificationMessage, nn string) (*operators.NotificationTask, error) {
	task := &operators.NotificationTask{
		Message:      nm,
		Notification: &aiopsv1beta1.Notification{},
		Reference: &operators.NotificationReference{
			Secrets:   []*v1.Secret{},
			Servers:   []*aiopsv1beta1.NotificationServer{},
			Receivers: []*aiopsv1beta1.NotificationReceiver{},
			Templates: []*aiopsv1beta1.NotificationTemplate{},
		},
	}

	// Get associated notification for this task
	if err := rm.Get(context.TODO(), types.NamespacedName{Name: nn, Namespace: nm.Namespace}, task.Notification); err != nil {
		return task, err
	}

	// Add referred resources for notification servers, senders, templates and receivers.
	for i := range task.Notification.Spec.Subscriptions {
		if err := addSenderReferences(rm, task.Reference, &task.Notification.Spec.Subscriptions[i]); err != nil {
			return task, err
		}
		if err := addTemplateReferences(rm, task.Reference, &task.Notification.Spec.Subscriptions[i]); err != nil {
			return task, err
		}

		if err := addReceiverReferences(rm, task.Reference, &task.Notification.Spec.Subscriptions[i]); err != nil {
			return task, err
		}
	}
	return task, nil
}

func addSenderReferences(rm *ReconcileMessage, nr *operators.NotificationReference, ss *aiopsv1beta1.NotificationSubscription) error {
	sender := ss.Sender
	if sender == "" {
		return nil
	}
	senderInstance := &v1.Secret{}
	if err := rm.Get(context.TODO(), types.NamespacedName{Namespace: aiopsv1beta1.ControllerNamespace, Name: sender}, senderInstance); err != nil {
		return err
	}
	nr.Secrets = append(nr.Secrets, senderInstance)
	server := senderInstance.Labels[aiopsv1beta1.LabelKeyServer]
	if server == "" {
		return nil
	}
	serverInstance := &aiopsv1beta1.NotificationServer{}
	if err := rm.Get(context.TODO(), types.NamespacedName{Name: server}, serverInstance); err != nil {
		return err
	}
	nr.Servers = append(nr.Servers, serverInstance)
	return nil
}

func addTemplateReferences(rm *ReconcileMessage, nr *operators.NotificationReference, ss *aiopsv1beta1.NotificationSubscription) error {
	template := ss.Template
	if template == "" {
		return nil
	}
	notificationTemplate := &aiopsv1beta1.NotificationTemplate{}
	if err := rm.Get(context.TODO(), types.NamespacedName{Name: template}, notificationTemplate); err != nil {
		return err
	}
	nr.Templates = append(nr.Templates, notificationTemplate)
	return nil
}

func addReceiverReferences(rm *ReconcileMessage, nr *operators.NotificationReference, ss *aiopsv1beta1.NotificationSubscription) error {
	receivers := ss.Receivers
	for _, r := range receivers {
		notificationReceiver := &aiopsv1beta1.NotificationReceiver{}
		if err := rm.Get(context.TODO(), types.NamespacedName{Name: r.Name, Namespace: r.Namespace}, notificationReceiver); err != nil {
			Logger.Error(err, "Get notificationreceiver error. Skip", "notificationreceiver", r.Name)
			continue
		}
		nr.Receivers = append(nr.Receivers, notificationReceiver)
	}
	return nil
}
