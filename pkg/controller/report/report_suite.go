package report

import (
	"strconv"
	"strings"
	"time"

	meteringV1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/metering/v1beta1"
	"bitbucket.org/mathildetech/courier/pkg/clients/morgans"
)

func GenerateReport(instance *meteringV1beta1.Report) error {
	report := NewReport(instance)
	switch instance.Spec.Kind {
	case MeterKindSummary:
		y, m, _ := instance.Spec.EndTime.Date()
		startTime := time.Date(y, m, 1, 0, 0, 0, 0, time.Local)
		endTime := startTime.AddDate(0, 1, 0).Add(-1)
		for {
			if startTime.Before(instance.Spec.StartTime.Time) {
				startTime = instance.Spec.StartTime.Time
			}
			if !endTime.After(startTime) {
				break
			}
			records, err := GetSummaryRecords(instance, startTime, endTime)
			if err != nil {
				return err
			}
			report.AddSummaryRecords(startTime.Format("2006-01"), records)
			startTime = startTime.AddDate(0, -1, 0)
			endTime = endTime.AddDate(0, -1, 0)
		}
	case MeterKindDetail:
		y, m, d := instance.Spec.EndTime.Date()
		startTime := time.Date(y, m, d, 0, 0, 0, 0, time.Local)
		endTime := startTime.AddDate(0, 0, 1).Add(-1)
		for {
			if startTime.Before(instance.Spec.StartTime.Time) {
				startTime = instance.Spec.StartTime.Time
			}
			if !endTime.After(startTime) {
				break
			}
			records, err := GetQueryRecords(instance, startTime, endTime)
			if err != nil {
				return err
			}
			report.AddQueryRecords(startTime.Format("2006-01-02"), records)
			startTime = startTime.AddDate(0, 0, -1)
			endTime = endTime.AddDate(0, 0, -1)
		}
	}
	return report.Upload()
}

func GetQueryRecords(instance *meteringV1beta1.Report, startTime, endTime time.Time) ([]*morgans.MeterQueryItem, error) {
	results := make([]*morgans.MeterQueryItem, 0)
	page := 0
	pageSize := 1000
	for {
		page += 1
		params := map[string]string{
			"type":      instance.Spec.Type,
			"groupBy":   instance.Spec.GroupBy,
			"startTime": startTime.Format("2006-01-02T15:04:05Z"),
			"endTime":   endTime.Format("2006-01-02T15:04:05Z"),
			"project":   strings.Join(instance.Spec.Projects, ","),
			"cluster":   strings.Join(instance.Spec.Clusters, ","),
			"namespace": strings.Join(instance.Spec.Namespaces, ","),
			"page":      strconv.Itoa(page),
			"pageSize":  strconv.Itoa(pageSize),
			"orderBy":   instance.Spec.OrderBy,
		}
		records, hasNext, err := morgans.Client.GetQuery(params)
		if err != nil {
			return nil, err
		}
		results = append(results, records...)
		if !hasNext {
			break
		}
	}
	return results, nil
}

func GetSummaryRecords(instance *meteringV1beta1.Report, startTime, endTime time.Time) ([]*morgans.MeterSummaryItem, error) {
	results := make([]*morgans.MeterSummaryItem, 0)
	page := 0
	pageSize := 1000
	for {
		page += 1
		params := map[string]string{
			"type":      instance.Spec.Type,
			"groupBy":   instance.Spec.GroupBy,
			"startTime": startTime.Format("2006-01-02T15:04:05Z"),
			"endTime":   endTime.Format("2006-01-02T15:04:05Z"),
			"project":   strings.Join(instance.Spec.Projects, ","),
			"cluster":   strings.Join(instance.Spec.Clusters, ","),
			"namespace": strings.Join(instance.Spec.Namespaces, ","),
			"page":      strconv.Itoa(page),
			"pageSize":  strconv.Itoa(pageSize),
			"orderBy":   instance.Spec.OrderBy,
		}
		records, hasNext, err := morgans.Client.GetSummary(params)
		if err != nil {
			return nil, err
		}
		results = append(results, records...)
		if !hasNext {
			break
		}
	}
	return results, nil
}
