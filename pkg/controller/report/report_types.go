package report

import (
	"encoding/csv"
	"os"
	"strings"

	meteringV1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/metering/v1beta1"
	"bitbucket.org/mathildetech/courier/pkg/clients/morgans"
)

const (
	MeterTotalPlaceholder     string = "######"
	MeterTypePodUsageEN       string = "podUsage"
	MeterTypePodRequestsEN    string = "podRequests"
	MeterTypeNamespaceQuotaEN string = "namespaceQuota"
	MeterTypeProjectQuotaEN   string = "projectQuota"
	MeterGroupByProjectEN     string = "project"
	MeterGroupByNamespaceEN   string = "namespace"
	MeterKindSummary          string = "summary"
	MeterKindDetail           string = "detail"

	MeterTypePodUsageCN       string = "容器组使用量"
	MeterTypePodRequestsCN    string = "容器组配额"
	MeterTypeNamespaceQuotaCN string = "命名空间配额"
	MeterTypeProjectQuotaCN   string = "项目配额"
	MeterGroupByProjectCN     string = "项目"
	MeterGroupByNamespaceCN   string = "命名空间"
)

var (
	ENToCN = map[string]string{
		MeterTypePodUsageEN:       MeterTypePodUsageCN,
		MeterTypePodRequestsEN:    MeterTypePodRequestsCN,
		MeterTypeNamespaceQuotaEN: MeterTypeNamespaceQuotaCN,
		MeterTypeProjectQuotaEN:   MeterTypeProjectQuotaCN,
		MeterGroupByProjectEN:     MeterGroupByProjectCN,
		MeterGroupByNamespaceEN:   MeterGroupByNamespaceCN,
	}
)

type Report struct {
	Object *meteringV1beta1.Report
	Meter  *morgans.MeterRecord
	Name   string
	Data   [][]string
}

func NewReport(object *meteringV1beta1.Report) *Report {
	// Init report and set name for report
	report := &Report{
		Object: object,
		Meter:  &morgans.MeterRecord{CPU: 0, Memory: 0},
		Name:   object.Name + ".csv",
		Data:   nil,
	}

	switch object.Spec.Kind {
	case MeterKindSummary:
		// Set report header, add place holder for meter total
		report.Data = [][]string{
			{object.Spec.StartTime.Format("200601") + "至" + object.Spec.EndTime.Format("200601") + "-" + ENToCN[object.Spec.Type] + "-按" + ENToCN[object.Spec.GroupBy] + "分组"},
		}
		if len(object.Spec.Projects) == 0 {
			report.Data = append(report.Data, []string{"项目统计范围：全部"})
		} else {
			report.Data = append(report.Data, []string{"项目统计范围：" + strings.Join(object.Spec.Projects, "，")})
		}
	case MeterKindDetail:
		// Set report header, add place holder for meter total
		project := ""
		if len(object.Spec.Projects) > 0 {
			project = object.Spec.Projects[0]
		}
		report.Data = [][]string{
			{object.Spec.StartTime.Format("20060102") + "至" + object.Spec.EndTime.Format("20060102") + ENToCN[object.Spec.Type] + "-" + project + "-计量明细表"},
		}
		printSlice := func(s []string) string {
			if len(s) == 0 {
				return "全部"
			} else {
				return strings.Join(s, "，")
			}
		}
		report.Data = append(report.Data, []string{"数据范围：" + printSlice(object.Spec.Projects) + "，集群：" + printSlice(object.Spec.Clusters) + "，命名空间：" + printSlice(object.Spec.Namespaces)})
	}
	report.Data = append(report.Data, []string{MeterTotalPlaceholder})
	object.Status.DownloadLink = "/" + morgans.Client.ApiVersion + "/meter/reports/" + report.Name
	return report
}

func (r *Report) AddSummaryRecords(month string, records []*morgans.MeterSummaryItem) {
	// Calculate monthly meter and add it to total
	monthTotal := morgans.MeterRecord{CPU: 0, Memory: 0}
	for _, record := range records {
		monthTotal.Add(&record.Meter)
	}
	r.Meter.Add(&monthTotal)
	// Set header for this month
	r.Data = append(r.Data, []string{month + ">"})
	r.Data = append(r.Data, []string{"月总计: CPU使用量" + monthTotal.GetCPU() + "核*小时，内存使用量" + monthTotal.GetMemory() + "GB*小时"})
	if r.Object.Spec.GroupBy == MeterGroupByProjectEN {
		r.Data = append(r.Data, []string{"项目名称", "项目显示名称", "CPU使用量（核*小时）", "内存使用量（GB*小时）"})
	} else {
		r.Data = append(r.Data, []string{"命名空间名称", "显示名称", "所属集群", "所属项目", "CPU使用量（核*小时）", "内存使用量（GB*小时）"})
	}
	// Add meter records to report, comma will be replaced by semicolon
	for _, i := range records {
		i.Project.DisplayName = strings.ReplaceAll(i.Project.DisplayName, ",", "，")
		i.Cluster.DisplayName = strings.ReplaceAll(i.Cluster.DisplayName, ",", "，")
		i.Namespace.DisplayName = strings.ReplaceAll(i.Namespace.DisplayName, ",", "，")
		if r.Object.Spec.GroupBy == MeterGroupByProjectEN {
			r.Data = append(r.Data, []string{i.Project.Name, i.Project.DisplayName, i.Meter.GetCPU(), i.Meter.GetMemory()})
		} else {
			r.Data = append(r.Data, []string{i.Namespace.Name, i.Namespace.DisplayName, i.Cluster.Name, i.Project.Name, i.Meter.GetCPU(), i.Meter.GetMemory()})
		}
	}
}

func (r *Report) AddQueryRecords(day string, records []*morgans.MeterQueryItem) {
	// Calculate daily meter and add it to total
	dayTotal := morgans.MeterRecord{CPU: 0, Memory: 0}
	for _, record := range records {
		dayTotal.Add(&record.Meter)
	}
	r.Meter.Add(&dayTotal)
	// Set header for this month
	r.Data = append(r.Data, []string{day + ">"})
	r.Data = append(r.Data, []string{"日总计: CPU使用量" + dayTotal.GetCPU() + "核*小时，内存使用量" + dayTotal.GetMemory() + "GB*小时"})
	r.Data = append(r.Data, []string{"容器组名称", "所属命名空间", "所属集群", "所属项目", "CPU使用量（核*小时）", "内存使用量（GB*小时）", "开始时间", "结束时间"})
	// Add meter records to report, comma will be replaced by semicolon
	for _, i := range records {
		r.Data = append(r.Data, []string{i.Pod, i.Namespace.Name, i.Cluster.Name, i.Project.Name, i.Meter.GetCPU(), i.Meter.GetMemory(), i.StartTime.Format("2006-01-02 15:04:05"), i.EndTime.Format("2006-01-02 15:04:05")})
	}
}

func (r *Report) Upload() error {
	// Set total meter for report
	for i, d := range r.Data {
		if len(d) == 1 && d[0] == MeterTotalPlaceholder {
			r.Data[i] = []string{"总计: CPU使用量" + r.Meter.GetCPU() + "核*小时，内存使用量" + r.Meter.GetMemory() + "GB*小时"}
		}
	}
	path := "/tmp/" + r.Name
	// Generate report csv file
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()
	if _, err := f.WriteString("\xEF\xBB\xBF"); err != nil {
		return err
	}
	w := csv.NewWriter(f)
	if err := w.WriteAll(r.Data); err != nil {
		return err
	}
	w.Flush()
	// Upload csv file and delete temp file
	if err := morgans.Client.UploadRequest(r.Name, path); err != nil {
		Logger.Error(err, "Upload request error", "report", r.Object.Name)
		return err
	}
	_ = f.Close()
	if err := os.Remove(path); err != nil {
		Logger.Error(err, "Delete file error", "report", r.Object.Name)
		return err
	}
	return nil
}
