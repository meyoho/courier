/*
Copyright 2019 Alauda AIOps Team.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package report

import (
	"context"
	"time"

	meteringV1beta1 "bitbucket.org/mathildetech/courier/pkg/apis/metering/v1beta1"

	"k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var Logger = log.Log.WithName("report_controller")

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new Report Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileReport{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("report-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to Meter Report
	err = c.Watch(&source.Kind{Type: &meteringV1beta1.Report{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileReport{}

// ReconcileReport reconciles a Report object
type ReconcileReport struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a Report object and makes changes based on the state read
// and what is in the Report.Spec
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=metering.alauda.io,resources=reports,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=metering.alauda.io,resources=reports/status,verbs=get;update;patch
func (r *ReconcileReport) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the Report instance
	instance := &meteringV1beta1.Report{}
	if err := r.Get(context.TODO(), request.NamespacedName, instance); err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}
	Logger.Info("Step into meter report reconcile", "report", instance.Name)

	// Check if the message has been handled
	if instance.Status.Phase == meteringV1beta1.PhaseCompleted || instance.Status.Phase == meteringV1beta1.PhaseFailed {
		Logger.Info("This report has been handled, skip", "report", instance.Name)
		return reconcile.Result{}, nil
	}

	// Update phase to processing and refresh instance
	if instance.Status.Phase != meteringV1beta1.PhaseProcessing {
		_, err := r.UpdateStatus(instance, "", "The report is in processing", meteringV1beta1.PhaseProcessing)
		after, _ := time.ParseDuration("1s")
		return reconcile.Result{RequeueAfter: after}, err
	}

	// Generate report, upload report and update status
	if err := GenerateReport(instance); err != nil {
		condition := r.GetReadyCondition(instance)
		condition.RetryTimes += 1
		if condition.RetryTimes > 5 {
			condition.RetryTimes = 5
			return r.UpdateStatus(instance, "Too many retries due to error", err.Error(), meteringV1beta1.PhaseFailed)
		} else {
			_, _ = r.UpdateStatus(instance, "", err.Error(), meteringV1beta1.PhaseProcessing)
			after, _ := time.ParseDuration("10s")
			return reconcile.Result{RequeueAfter: after}, nil
		}
	}
	return r.UpdateStatus(instance, "", "", meteringV1beta1.PhaseCompleted)
}

func (r *ReconcileReport) GetReadyCondition(instance *meteringV1beta1.Report) *meteringV1beta1.ReportCondition {
	if len(instance.Status.Conditions) == 0 {
		instance.Status.Conditions = []meteringV1beta1.ReportCondition{}
	}

	for i, c := range instance.Status.Conditions {
		if c.Type == meteringV1beta1.ReporterReady {
			return &instance.Status.Conditions[i]
		}
	}

	c := meteringV1beta1.ReportCondition{
		Type:               meteringV1beta1.ReporterReady,
		Status:             meteringV1beta1.ConditionFalse,
		LastProbeTime:      metaV1.Now(),
		LastTransitionTime: metaV1.Now(),
		RetryTimes:         0,
		Reason:             "",
		Message:            "",
	}
	instance.Status.Conditions = append(instance.Status.Conditions, c)
	return &c
}

func (r *ReconcileReport) UpdateStatus(instance *meteringV1beta1.Report, reason string, message string, phase string) (reconcile.Result, error) {
	condition := r.GetReadyCondition(instance)
	mirror := condition.DeepCopy()
	instance.Status.Phase = phase
	condition.Reason = reason
	condition.Message = message
	condition.LastProbeTime = metaV1.Now()
	if phase == meteringV1beta1.PhaseCompleted {
		condition.Status = meteringV1beta1.ConditionTrue
	}
	if mirror.Status != condition.Status {
		condition.LastTransitionTime = metaV1.Now()
	}

	if err := r.Status().Update(context.Background(), instance); err != nil {
		Logger.Error(err, "Update status error", "report", instance.Name)
		after, _ := time.ParseDuration("10s")
		return reconcile.Result{RequeueAfter: after}, nil
	}
	return reconcile.Result{}, nil
}
