package envs

import (
	"io/ioutil"

	"github.com/vrischmann/envconfig"
)

var (
	GlobalConfig Config
)

type Config struct {
	Courier struct {
		Debug          bool `envconfig:"default=false"`
		WebhookEnabled bool `envconfig:"default=false"`
	}

	Test struct {
		Enabled                bool   `envconfig:"default=false"`
		ProxyAddress           string `envconfig:"default=null"`
		ProxyAuth              string `envconfig:"default=null"`
		KubernetesEndpoint     string `envconfig:"default=https://kubernetes.default.svc.cluster.local"`
		KubernetesToken        string `envconfig:"default=token"`
		KubernetesTimeout      int    `envconfig:"default=10"`
		KubernetesWatchTimeout int    `envconfig:"default=600"`
	}

	Label struct {
		BaseDomain string `envconfig:"default=alauda.io"`
	}
}

func init() {
	var tokenFile = "/var/run/secrets/kubernetes.io/serviceaccount/token"

	if err := envconfig.Init(&GlobalConfig); err != nil {
		panic("Load config from env error," + err.Error())
	}

	if token, err := ioutil.ReadFile(tokenFile); err == nil {
		GlobalConfig.Test.KubernetesToken = string(token)
	}
}
